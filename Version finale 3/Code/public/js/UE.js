console.log("In UE.js");
/** Création de la classe UE et fonctions pour faire des appels à LiUE car on perds l'info coté serveur de temps en temps (souci nodejs?)
 *
 */

//Création de la classe UE
class UE {
  constructor(code, nom, effectif, nbCM, nbTD, nbTP, liGroupe, Color) {
    this.CodeUE = code;
    this.Nom = nom;
    this.effectif = effectif;
    this.CM = nbCM;
    this.TD = nbTD;
    this.TP = nbTP;
    this.color = Color;
    this.ListeGroupe = liGroupe;

    //this.Creneaux = [];
    for (var i = 0; i < liGroupe.length; i++) {
      //pour nbr de groupe associé a l'UE faire
      var t = liGroupe[i]; // on stocke le nom du groupe i
      this.ListeGroupe[i] = t;
    }
    //console.log("init value = = ", sessionStorage['init']);
    if (sessionStorage[code] != undefined) {
      this.H_posed = JSON.parse(sessionStorage[code]);
    } else {
      this.H_posed = [];
      for (var i = 0; i < liGroupe.length; i++) {
        this.H_posed.push([0, 0, 0]);
      }
    }
    // console.log(this.H_posed);
  }
  reset() {
    for (let i = 0; i < this.H_posed.length; i++) {
      for (let j = 0; j < 3; j++) {
        this.H_posed[i][j] = 0;
      }
    }
  }
  getNom() {
    return this.Nom;
  }
  gethposed() {
    return this.H_posed;
  }
  //Ajoute un CM à tous les groupes affectés à cette UE
  ajoutCM() {
    var x = get_x_format();
    for (var i = 0; i < this.ListeGroupe.length; i++) {
      //this.Creneaux[this.ListeGroupe[i]]["CM"].push(date);
      //J'y ajoute 1 si on vient de poser 1 bloc d'une heure ou 1,5 sinon.
      this.H_posed[i][0] += x;
      //console.log(this.H_posed[this.ListeGroupe[i]]["CM"]);
    }
  }
  retraitCM() {
    var x = get_x_format();
    for (var i = 0; i < this.ListeGroupe.length; i++) {
      //this.Creneaux[this.ListeGroupe[i]]["CM"].push(date);
      //J'y ajoute 1 si on vient de poser 1 bloc d'une heure ou 1,5 sinon.
      this.H_posed[i][0] -= x;
      //console.log(this.H_posed[this.ListeGroupe[i]]["CM"]);
    }
  }
  //Ajoute un CM à un groupe particulier
  ajoutCMGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][0] += x;
  }
  //Ajoute un TD à un groupe particulier
  ajoutTDGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][1] += x;
  }

  retraitTDGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][1] -= x;
  }
  //Ajoute un TP à un groupe particulier
  ajoutTPGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][2] += x;
  }

  retraitTPGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][2] -= x;
  }
  ajoutbloc(type, groupe) {
    if (type == "CM") {
      return this.ajoutCM();
    } else if (type == "TD") {
      return this.ajoutTDGroupe(groupe);
    } else if (type == "TP") {
      return this.ajoutTPGroupe(groupe);
    }
  }
  retraitbloc(type, groupe) {
    if (type == "CM") {
      return this.retraitCM();
    } else if (type == "TD") {
      return this.retraitTDGroupe(groupe);
    } else if (type == "TP") {
      return this.retraitTPGroupe(groupe);
    }
  }

  CMrestant() {
    return this.CM;
  }
  getblocrestant(type, groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    if (type == "CM") {
      return this.CM / x - this.H_posed[0][0] / x; // [0] est donc ici arbitraire
    } else if (type == "TD") {
      return this.TD / x - this.H_posed[i][1] / x;
    } else if (type == "TP") {
      return this.TP / x - this.H_posed[i][2] / x;
    }
  }
  getnbblocmax(type) {
    var x = get_x_format();
    if (type == "CM") {
      return this.CM / x;
    } else if (type == "TD") {
      return this.TD / x;
    } else if (type == "TP") {
      return this.TP / x;
    }
  }
}

var liGroupe = ["A", "B", "C", "D"]; // Static version pour test
// Liste des 3 dégradés de couleurs pour 9 UEs
var red = ["#8B0000", "#E9967A", "#FFA07A"];
var cyan = ["#008080", "#20B2AA", "#AFEEEE"];
var green = ["#006400", "#9ACD32", "#ADFF2F"];
var yellow = ["#FFD700", "#FFFF00", "#F0E68C"];
var purple = ["#483D8B", "#BA55D3", "#E6E6FA"];
var rose = ["#C71585", "#FF69B4", "#FFC0CB"];
var brown = ["#800000", "#BC8F8F", "#D2B48C"];
var orange = ["#FFA500", "#FF7F50", "#F4A460"];
var blue = ["#1E90FF", "#00BFFF", "#87CEEB"];
var LiCouleur = [red, green, orange, blue, yellow, purple, cyan, brown, rose];
//console.log("Voici la liste des groupes ", liGroupe);
function hey() {
  return "hello";
}
var liCodeUe = new Object();
var LiUE = [];
var LiCode = [];

// Fonction qui pour une liste de groupe va faire appel a importUE.json pour créer des instances UE avec la liste de groupe adéquates
function recupJSON(Liste_Groupe) {
  $.ajaxSetup({
    async: false,
  });

  $.getJSON("./json/importUE.json", function (data) {
    var i = 0;

    $.each(data, (_index, objet) => {
      LiCode.push(objet["Code UE"]);
      LiUE.push(
        new UE(
          objet["Code UE"],
          objet["Libellé"],
          objet["Nbre d'inscrits"],
          objet["CM"],
          objet["TD"],
          objet["TP"],
          Liste_Groupe[i],
          LiCouleur[i]
        )
      );
      i++;
    });
    $.ajaxSetup({
      async: true,
    });

    if (sessionStorage["init"] != "true") {
      sessionStorage.setItem("init", "true");
    }
  });
  return LiUE;
}

function getLiUE(Liste_Groupe) {
  // Recupère la liste des UEs LiUE
  recupJSON(Liste_Groupe);
  return LiUE;
}

function getLiCode() {
  // Recupère la liste des codeUEs LiCode
  return LiCode;
}

// Old recupCode
function recupCode() {
  //  console.log("Enter recupJSON(): ");
  $.ajaxSetup({
    async: false,
  });

  var LiUE = new Object();
  var liCodeUe = new Object();
  $.getJSON("./json/importUE.json", function (data) {
    $.each(data, (_index, objet) => {
      LiUE[objet.CodeUE] = new UE(
        objet.CodeUE,
        objet.NomUE,
        objet.CM,
        objet.TD,
        objet.TP,
        liGroupe,
        sessionStorage["init"],
        objet.C
      );
      liCodeUe[objet.CodeUE] = objet.CodeUE;
    });
    $.ajaxSetup({
      async: true,
    });
  });

  return liCodeUe;
}
