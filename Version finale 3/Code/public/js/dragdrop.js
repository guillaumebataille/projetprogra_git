console.log("In dragdrop.js");
/* Fonction appelé lors d'un drag&drop
 */
sessionStorage.id = 0;

function dragdrop(LiUE) {
  return {
    itemSelector: ".fc-event",
    eventData: function (eventEl) {
      var x = sessionStorage["format"];

      console.log("voici l'event El", eventEl);
      var UE = eventEl.attributes.id.value;
      var Color = LiUE[UE].color;
      var t;
      if (eventEl.innerText.substr(0, 2) == "CM") {
        return {
          title: eventEl.innerText,
          duration: x,
          UE: LiUE[UE],
          Informatise: 0,
          Intervenant: "",
          Libellé: LiUE[UE].LiUE,
          CodeUE: LiUE[UE].CodeUE,
          Type: eventEl.innerText.substr(0, 2),
          Groupe: "all",
          backgroundColor: Color[0],
          borderColor: "#000000",
          textColor: "#000000",
        };
      }
      if (eventEl.innerText.substr(0, 2) == "TD") {
        return {
          title: eventEl.innerText,
          duration: x,
          UE: LiUE[UE],
          Informatise: 0,
          Intervenant: "",
          Libellé: LiUE[UE].Libellé,
          CodeUE: LiUE[UE].CodeUE,
          Type: eventEl.innerText.substr(0, 2),
          Groupe: eventEl.innerText.split("-")[1].split(" ")[0],
          backgroundColor: Color[1],
          borderColor: "#000000",
          textColor: "#000000",
        };
      }
      if (eventEl.innerText.substr(0, 2) == "TP") {
        return {
          title: eventEl.innerText,
          duration: x,
          UE: LiUE[UE],
          Informatise: 0,
          Intervenant: "",
          Libellé: LiUE[UE].Libellé,
          CodeUE: LiUE[UE].CodeUE,
          Type: eventEl.innerText.substr(0, 2),
          Groupe: eventEl.innerText.split("-")[1].split(" ")[0],
          backgroundColor: Color[2],
          borderColor: "#000000",
          textColor: "#000000",
        };
      }
    },
  };
}
