let express = require("express");
// declaration d'express
let app = express();
let session = require("express-session");

// declaration des external_script
var ex = require("./external_script/Export");
var imp = require("./external_script/Import");
var save = require("./external_script/saving");
var load = require("./external_script/loading");

const { waitForDebugger } = require("inspector");
const { format } = require("path");
const { stripVTControlCharacters } = require("util");
// declaration de variable utilisés dans server.js
let LiCren; // Liste Crenaux
let LiFer; // Liste Ferié
var binf; // Borne inf
var bsup; // Borne sup
var vac = []; // Vacances
var UEfi; // filtre à UE
var array_groupe = []; // Liste groupe
var SS = "0"; // Semainier Type si il y a loading
var LiMal = "0"; //Semainier Malléable si il y a loading
var uniqueUE; // Liste des codes UEs unique si il y a loading
let err;

//Moteur de template
app.set("view engine", "ejs");

//Paramètrage du serveur
app.use("/assets", express.static("public"));
app.use(express.static("public"));

app.use("/js", express.static(__dirname + "public/js"));
app.use("/css", express.static(__dirname + "public/css"));
app.use("/json", express.static(__dirname + "public/json"));

// Extension de la mémoire pour gérer les cas ou il y a énormément de créneaux
app.use(express.urlencoded({ extended: false, limit: "50mb" }));
app.use(express.json({ limit: "50mb" }));

// Definition de la session
app.use(
  session({
    secret: "yoyo",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false },
  })
);

//Routes interne à l'application et appel aux scripts externes

//Route vers le formulaire (page d'accueuil du site)
app.get("/", (request, response) => {
  if (request.query.error == undefined) {
    err = 1;
  } else {
    err = request.query.error;
  }
  console.log(err);

  response.render("pages/form", {
    link: "s_mal.ejs",
    page: "Semainier - TYPE",
    err: err,
  });
});

// Route vers semainier malléable (via le bouton sur le semainier type)
app.get("/s_mal.ejs", (request, response) => {
  response.render("pages/s_mal", {
    link: "s_type.ejs",
    page: "Semainier - MALLEABLE",
    binf: binf,
    bsup: bsup,
    vac: [vac],
    UEfi: UEfi,
    Liste_Groupe: [array_groupe],
    SS: SS,
    LiMal: LiMal,
  });
});

//Route vers semainier type (depuis la page semainier Malléable) s_type
app.get("/s_type.ejs", (request, response) => {
  response.render("pages/s_type", {
    link: "s_mal.ejs",
    page: "Semainier - TYPE",
    Liste_Groupe: [array_groupe],
    SS: SS,
    LiMal: LiMal,
  });
});

// Route vers semainier type (depuis la page de vérification)
app.post("/verif.ejs", (request, response) => {
  binf = request.body.binf;
  bsup = request.body.bsup;
  vac = request.body.vac;
  grp = request.body.grp;
  grptype = request.body.grpglobal; // Le format de groupe (un groupe pour tout les UEs ou un groupe par UE)

  array_groupe = [];

  if (grptype == 1) {
    imp.filtreUE(UEfi, uniqueUE).forEach((UE) => {
      if (typeof grp == typeof "a") grp = [grp];
      array_groupe.push(grp);
      array_groupe.push("$");
    });
  } else {
    array_groupe = [];
    imp.filtreUE(UEfi, uniqueUE).forEach((UE) => {
      var i = imp.filtreUE(UEfi, uniqueUE).indexOf(UE).toString();
      var x =
        typeof request.body["grp" + i.toString()] == typeof "a"
          ? [request.body["grp" + i.toString()]]
          : request.body["grp" + i.toString()];

      array_groupe.push(x);
      array_groupe.push("$");
    });
    // Ajout de $ pour pouvoir split chaque nouveau groupe
    //en un tableau contenant a chaque case un groupe différent
    // car json à du mal avec l'envoi côté client des arrays.
  }

  response.render("pages/s_type", {
    link: "s_mal.ejs",
    page: "Semainier - TYPE",
    Liste_Groupe: [array_groupe],
    SS: SS,
    LiMal: LiMal,
  });
});

// Route vers la page de vérification (depuis le formulaire)
app.post("/", (req, res) => {
  console.log("Working");

  binf = req.body.borneinf;
  bsup = req.body.bornesup;
  vac = req.body.vac;
  UEfi = req.body.UEfilter;
  heure = req.body.format;
  if (typeof req.body.ue == "string" && req.body.ue != undefined) {
    uniqueUE = [req.body.ue];
  } else {
    uniqueUE = req.body.ue;
  }
  console.log("UniqueUE: ", uniqueUE);

  var UEfiltred = imp.filtreUE(UEfi, uniqueUE);
  var UEfil = [];
  var LisUE = [];
  UEfiltred.forEach((element) => {
    UEfil.push(element["Libellé"]);
    LisUE.push(element["Code UE"]);
  });
  if (UEfiltred.length == 0) {
    // Erreur car aucun UE n'a pu être chargé avec le filtre fourni
    res.redirect("/?error=" + UEfiltred.length.toString());
  } else {
    res.render("pages/verif", {
      binf: binf,
      bsup: bsup,
      vac: vac,
      UEfiltred: UEfil,
      LisUE: LisUE,
      format: heure,
      SS: SS,
      LiMal: LiMal,
    });
  }
});

// Appel a l'export (depuis le bouton sur le semainier malléable)
app.post("/listecrenaux", (req, res) => {
  LiCren = JSON.parse(req.body.listecrenaux);
  LiFer = JSON.parse(req.body.listeferie);
  ex.generateExcel(LiCren, LiFer, binf, bsup, vac);
  console.log("jours féries", LiFer);
  console.log("Successful export");
  res.json({ ok: true });
});

// Appel au chargement et route vers semainier malléable (depuis  le bouton "chargement" sur le formulaire)
app.post("/loading", (req, response) => {
  var resp = load.loading();
  binf = resp[0];
  bsup = resp[1];
  vac = resp[2];
  array_groupe = resp[4];
  SS = resp[5];
  LiMal = resp[6];
  uniqueUE = resp[7];
  UEfiltred = imp.filtreUE(resp[3], uniqueUE); // Y ajouter la liste d'ue unique si besoin

  response.render("pages/s_mal", {
    link: "s_type.ejs",
    page: "Semainier - MALLEABLE",
    binf: binf,
    bsup: bsup,
    vac: [vac],
    UEfi: UEfi,
    Liste_Groupe: [array_groupe],
    SS: SS,
    LiMal: LiMal,
  });
  SS = "0";
  LiMal = "0";
  // 0 = binf // 1 = bsup // 2 = vac // 3 = UEfi // 4 = listegroupe // 5 = SessionS // 6 = ListeCrenauxMalleable
});

// Appel a saving.js (via le bouton saving sur le semainer malléable)
app.post("/saving", (req, res) => {
  var LiUEfiltre = req.body.LiUEfiltre;
  var SessionSto = req.body.sessionS;
  console.log("SessionSto ", SessionSto);
  var listemal = req.body.listecrenaux;
  var array_saved = save.saving(
    binf,
    bsup,
    vac,
    LiUEfiltre,
    array_groupe,
    SessionSto,
    listemal,
    uniqueUE
  );
  // 0 = binf // 1 = bsup // 2 = vac // 3 = UEfi // 4 = listegroupe // 5 = SessionS // 6 = ListeCrenauxMalleable
});

// Ecoute sur le port donné
app.listen(8080);
