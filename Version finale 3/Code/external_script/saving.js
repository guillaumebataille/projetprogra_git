var fs = require("fs");
/*
Script pour sauvegarder une instance de semainier malléable dans un json appelé saving.json
 */
console.log("In saving.js");

var i = 0;
function saving(
  binf,
  bsup,
  vac,
  FiltreUE,
  listegroupe,
  SessionS,
  listemal,
  uniqueUE
) {
  var x = [];
  var filtreUE = FiltreUE;
  var listegrp = listegroupe;
  var uniqueue = uniqueUE;

  x.push(binf, bsup, vac, filtreUE, listegrp, SessionS, listemal, uniqueue);
  console.log("TAILLE ;", x.length);
  try {
    fs.unlinkSync("./public/json/saving.json");
    console.log("Suppresion " + i.toString());
  } catch (err) {
    console.error(err);
  }

  fs.writeFileSync("./public/json/saving.json", JSON.stringify(x)); //writing
  console.log("ecriture " + i.toString());
  i++;
  return x;
}

exports.saving = saving;
