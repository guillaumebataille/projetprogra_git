let fs = require("fs");

/*
Fonction du cours de pompidor pour pouvoir récupérer le JSON.
*/
let recupJSON = function () {
  var texte = fs.readFileSync("./public/json/importUE.json", "utf8");
  let data = JSON.parse(texte);
  console.log("recupJSON successful");
  return data;
};

module.exports = recupJSON;
