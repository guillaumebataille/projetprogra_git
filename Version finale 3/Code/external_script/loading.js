var fs = require("fs");
/*
Script pour effectuer le chargement depuis le json sauvegardé saving.json
 */
console.log("In loading.js");

function loading() {
  var texte = fs.readFileSync("./public/json/saving.json", "utf8");
  var x = JSON.parse(texte);
  return x;
}

exports.loading = loading;
