console.log('In UE.js');
//Création de la classe UE
class UE {
  constructor(code, nom, effectif,nbCM, nbTD, nbTP, liGroupe, Color) {
    this.CodeUE = code;
    this.Nom = nom;
    this.effectif = effectif;
    this.CM = nbCM;
    this.TD = nbTD;
    if (nbTP === NaN){
      this.TP = 0;
    }
    else{
    this.TP = nbTP;}
    this.color = Color;
    this.ListeGroupe = [];
    //this.Creneaux = [];
    for (var i = 0; i < liGroupe.length; i++) { //pour nbr de groupe associé a l'UE faire
      var t = liGroupe[i]; // on stocke le nom du groupe i
      this.ListeGroupe[i] = t;
    }
    //console.log("init value = = ", sessionStorage['init']);
    if (sessionStorage[code] != undefined) {
      this.H_posed = JSON.parse(sessionStorage[code]);
    }
    else {
  //    console.log('constructing a new H_posed');
      this.H_posed = [];
      for (var i = 0; i < liGroupe.length; i++) {
        this.H_posed.push([0, 0, 0]);
      }
    }
  }
  getNom() {
    return this.Nom;
  }
  gethposed() {
    return this.H_posed;
  }
  //Ajoute une CM à tous les groupes affectés à cette UE
  ajoutCM() {
    var x = get_x_format();
    for (var i = 0; i < this.ListeGroupe.length; i++) {
      //this.Creneaux[this.ListeGroupe[i]]["CM"].push(date);
      //J'y ajoute 1 si on vient de poser 1 bloc d'une heure ou 1,5 sinon.
      this.H_posed[i][0] += x;
      //console.log(this.H_posed[this.ListeGroupe[i]]["CM"]);
    }
  }
  //Ajoute un CM à un groupe particulier
  ajoutCMGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][0] += x;
  }
  //Ajoute un TD à un groupe particulier
  ajoutTDGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][1] += x;
  }
  //Ajoute un TP à un groupe particulier
  ajoutTPGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][2] += x
  }
  ajoutbloc(type, groupe) {
    if (type == 'CM') {
      return this.ajoutCM();
    } else if (type == 'TD') {
      return this.ajoutTDGroupe(groupe);
    } else if (type == 'TP') {
      return this.ajoutTPGroupe(groupe);
    }
  }
  CMrestant() {
    return this.CM;
  }
  getblocrestant(type, groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    if (type == 'CM') {
      return this.CM / x - this.H_posed[i][0] / x; // [0] est donc ici arbitraire
    }
    else if (type == 'TD') {
      return this.TD / x - this.H_posed[i][1] / x;
    }
    else if (type == 'TP') {
      return this.TP / x - this.H_posed[i][2] / x;
    }
  }
  getnbblocmax(type) {

    var x = get_x_format();

    if (type == 'CM') {
      return this.CM / x;
    }
    else if (type == 'TD') {
      return this.TD / x;
    }
    else if (type == 'TP') {
      return this.TP / x;
    }
  }
};


var liGroupe = ["A", "B", "C", "D"];
console.log("Voici la liste des groupes ", liGroupe);
function hey(){
  return "hello";
}
var liCodeUe = new Object();
var LiUE = [];
var LiCode = [];

function recupJSON() {
  console.log("In recupJSON")
  
  //console.log("Enter recupJSON(): ");
  $.ajaxSetup({
    async: false
});

  $.getJSON('./json/importUE.json', function (data) {
    $.each(data, (_index, objet) => {
    LiCode.push(objet["Code UE"]);
    LiUE.push(new UE(objet["Code UE"],objet["Libellé"],objet["Nbre d'inscrits"],objet["CM"],objet["TD"],objet["TP"],liGroupe,"#b5ced4"));
    //new UE(objet.CodeUE, objet.NomUE, objet.CM, objet.TD, objet.TP, liGroupe, sessionStorage["init"],objet.C);
    console.log(objet.TP);
    })
    $.ajaxSetup({
      async: true
  });
  
    if (sessionStorage['init'] != 'true') {
      sessionStorage.setItem("init", "true");
    }
  })
  return LiUE
};

function getLiUE(){
  recupJSON();
  return LiUE;
}

function getLiCode(){
  return LiCode;
}
//exports.recupJSON = recupJSON;

function recupCode() {
//  console.log("Enter recupJSON(): ");
  $.ajaxSetup({
    async: false
});

  var LiUE = new Object;
  var liCodeUe = new Object;
  $.getJSON('./json/importUE.json', function (data) {
    $.each(data, (_index, objet) => {
      LiUE[objet.CodeUE] = new UE(objet.CodeUE, objet.NomUE, objet.CM, objet.TD, objet.TP, liGroupe, sessionStorage["init"],objet.C);
      liCodeUe[objet.CodeUE] = objet.CodeUE;
      //console.log("Creation de LiUE :", LiUE);
      //console.log("Creation de liCodeUe :", liCodeUe);
    })
    $.ajaxSetup({
      async: true
  });
  
  })

  return liCodeUe;
};

