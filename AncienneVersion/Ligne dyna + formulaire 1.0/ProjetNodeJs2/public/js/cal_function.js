console.log("In cal_function.js");

//import { Calendar } from "./main";
//import * as cal from './main.js';

export function add(calendar) {
  calendar.addEvent({
    start: "2022-03-28T10:30:00",
    end: "2022-03-28T11:30:00",
  });
  return console.log("Done");
}

export function adding_ferie(calendar) {
  all_ferie.forEach((item) => {
    calendar.addEvent({
      start: toY_M_D(item),
      end: toY_M_D(item),
      display: "background",
      color: "green",
      overlap: false,
    });
  });
}

export function adding_vac(calendar, vac) {
  vac.forEach((item) => {
    var d = new Date(item);
    var start, end;
    var start = WeekDay(d, 1); //Lundi
    var end = WeekDay(d, 6); //Vendredi
    calendar.addEvent({
      start: toY_M_D(start),
      end: toY_M_D(end),
      display: "background",
      color: "blue",
      overlap: false,
    });
  });
}

export function liste_Crenaux(calendar) {
  var listCrenaux = [];
  calendar.getEvents().forEach((event) => {
    // On prends tout les events
    if (event._def.ui.display != "background") {
      // et on exclu ceux qui ont la propriété background (reservé au ferié et autres choses spécifiques)
      listCrenaux.push(event); //On mets le tout dans une array
    }
  });
  return listCrenaux;
}

export function liste_ferie(calendar) {
  var res = [];
  calendar.getEvents().forEach((event) => {
    if (
      event._def.ui.display == "background" &&
      event._def.ui.backgroundColor == "red"
    ) {
      res.push(event._instance.range.start);
    }
  });
  return res;
}

export function valid_ferie(calendar, binf, bsup) {
  var res = liste_ferie(calendar);
  var x = [];
  res.forEach((f) => {
    if (is_valid_date(f, binf, bsup)) {
      x.push(f);
    }
  });
  return x;
}

export function liste_vac(calendar) {
  var res = [];
  calendar.getEvents().forEach((event) => {
    var duo = [];
    if (
      event._def.ui.display == "background" &&
      event._def.ui.backgroundColor == "blue"
    ) {
      duo.push(event._instance.range.start);
      duo.push(event._instance.range.end);
      res.push(duo);
    }
  });
  return res;
}

export function callback(liCodeUe, LiUE, e, calendar) {
  var e = window.e || e;
  sessionStorage.setItem(
    "Mes_Crenaux",
    JSON.stringify(liste_Crenaux(calendar))
  );

  for (var i in liCodeUe) {
    var a = LiUE;

    var b = Object.entries(a);
    var c = Object.values(a);

    var d = JSON.stringify(b);
    var e = JSON.stringify(c);
    //console.log("Version JSON stringify : entries = ", d, " values = ",e);
    sessionStorage.setItem[("A", d)];
    sessionStorage.setItem[("B", e)];
    var f = sessionStorage["A"];
    var g = sessionStorage["B"];

    var w = a[i].gethposed();
    var y = JSON.stringify(w);
    //console.log("La version stringify ", y);
    sessionStorage.setItem(i, y);
  }

  sessionStorage.setItem("ListeUE", JSON.stringify(LiUE));
  var a = JSON.parse(sessionStorage["ListeUE"]);
}

export function repeat_event_import(
  calendar,
  event_array,
  datee,
  n,
  vac,
  bsup,
  binf
) {
  //Ajout des vacances
  var date = new Date(datee.getTime());
  console.log("VOICI LA DATE A LA BASE DE REPEATEVENIMPORT : ", date);
  //Il faut mettre datestart au jour de la semaine correspondant a l'event
  var j = new Date(event_array[0].start).getDay(); //C'est le jour de la semaine ou l'event a lieu de base.
  var datestart = WeekDay(date, j);
  let startinit = new Date(fuse_timestamp(datestart, event_array[0].start));
  let endinit = new Date(fuse_timestamp(datestart, event_array[0].end));
  //L'heure -2 car ça fonctionne pas sinon (decalage +2 via une conversion que je ne connais pas)
  let starthours = startinit.getHours();
  let endhours = endinit.getHours();
  //Les datas a associé a tout mes elements
  let data = event_array[0].extendedProps;
  let title = event_array[0].title;
  var arrlen = event_array.length; // voici la taille de l'array
  var i = 0;
  var k = 0;
  console.log(data, title, arrlen, bsup);
  //on est donc ici supposé etre a une date valide.
  while (!outofsemester(startinit, bsup) && i < n) {
    // tant qu'on arrive pas a la fin du semestre et que i est inferieur au nbr d'heure a posé
    console.log(
      "La date est elle valide ? : ",
      is_valid_date(startinit, binf, bsup),
      "et ",
      i,
      "<",
      n,
      " ? ",
      i < n
    );
    console.log(!vacances_date(startinit, vac));
    if (
      !ferie_date(startinit) &&
      is_valid_date(startinit, binf, bsup) &&
      !vacances_date(startinit, vac)
    ) {
      // si on est pas sur un jour ferié et que la date est valide, on peut poser un crenaux et passer au prochaine de la liste
      console.log("La date de pause n'est pas ferié et pas vacance");
      calendar.addEvent({
        title: title + "-" + i.toString(), // Juste pour afficher le numero de l'UE qu'on a créer
        start: startinit.toISOString(),
        end: endinit.toISOString(),
        extendedProps: data,
        backgroundColor: getcolor(event_array[0]),
        borderColor: getcolorborder(event_array[0]),
      });
      i++; // i represente le nbr de bloc effectivement posé
      console.log(
        "Un nouvel element a été ajouté ! Il y en a actuellement :",
        i
      );
      k++;
      console.log("On passe a l'element suivant en incrémentant k ", k);
    } else {
      k++;
      console.log("On passe a l'element suivant en incrémentant k ", k); // k represente l'essai de mise dans la case
    }
    // peut importe si on a reussi a mettre l'element, on regarde le prochain.
    console.log("Que vaut ", k, "%", arrlen, " = ", k % arrlen);
    if (k % arrlen == 0) {
      // si on reviens a la premiere date de notre array a poser, alors on ajoute une semaine a cette date de base
      console.log("On passe donc a la semaine d'apres: ", date);
      date.setDate(date.getDate() + 7);
      console.log("Que voici : ", date);
    }
    console.log(
      "voici le jour ou on a posé l'ancien bloc",
      j,
      " et voici la date associé: ",
      datestart
    );
    j = new Date(event_array[k % arrlen].start).getDay();
    console.log("voici le jour ou on va poser le nouveau bloc ", j);
    datestart = WeekDay(date, j);
    console.log("voici la nouvelle date associé", datestart);
    console.log("voici les anciens debut et fin : ", startinit, endinit);
    startinit = new Date(
      fuse_timestamp(datestart, event_array[k % arrlen].start)
    );
    endinit = new Date(fuse_timestamp(datestart, event_array[k % arrlen].end));
    console.log("voici les nouveaux ", startinit, endinit);
    starthours = startinit.getHours();
    console.log();
    endhours = endinit.getHours();
    console.log();
    data = event_array[k % arrlen].extendedProps;
    console.log(data);
    title = event_array[k % arrlen].title;
    console.log(title);
    // et on continue TANT QUE : la date de debut est valide (pas hors du semestre), et tant que i, le nbr d'element effectivement posé dans le semainier est inferieur au nbr max a poser
  }
  return i;
  // return i; // retourne le nbr de fois ou un bloc a effectivement bien été posé
}

export function repeat_from_type(
  calendar,
  LiUE,
  LiCode,
  liste_droped_imported,
  datee, //binf
  vac,
  bsup
) {
  var liGroupe = ["A", "B", "C", "D"];
  var binf = datee;
  var li = LiCode;
  // console.log(recupJSON());
  var lity = ["CM", "TD", "TP"];
  var lastdatestart;
  var date = new Date(datee);
  console.log(datee);
  var copiedDate = new Date(date.getTime());
  console.log("Voici copiedtime: ", copiedDate);
  //console.log("Starting repeating from type to second : ", LiUE);
  LiCode.forEach((code) => {
    //var code = idcode;
    console.log(code);
    for (var idtype in lity) {
      var type = lity[idtype];
      console.log("voici le type qu'on etudie ", type);
      ///////////////////////////////////////////////
      if (type == "CM") {
        console.log(type, "on est bien avec un CM");
        var groupe = "A";
        console.log("Liste drop imp ", liste_droped_imported);
        var x = cptblocidentique(liste_droped_imported, code, type, groupe);
        console.log(
          "voici le nbr de bloc identique caractérisé par ",
          code,
          type,
          groupe,
          " = ",
          x
        );
        if (x > 0) {
          var datemoved = date;
          console.log("voici la date avant deplacement : ", datemoved);
          var A = getblocidentique(liste_droped_imported, code, type, groupe);
          console.log("On trie A", A);
          A.sort(compare);
          console.log(
            "Liste des blocs identique a des horaires differentes : ",
            A
          );
          var B = A[0];
          console.log(
            "Le premier element de la liste des blocs identiques :",
            B
          );
          var C = B.extendedProps.CodeUE;
          console.log("Le code UE de cet element : ", C);
          //var D = A[0].extendedProps.UE;
          var D = LiUE.find((e) => e.CodeUE == C);
          console.log(
            "Accès a l'objet UE via l'attribut UE transmis au crénaux : ",
            D
          );
          var dump = new UE(
            code,
            "newUE",
            D.effectif,
            D.CM,
            D.TD,
            D.TP,
            liGroupe,
            D.Color
          );
          console.log(
            "Generation d'un UE identique au precedent dont les méthodes sont utilisable :",
            dump
          );
          var E = dump.getblocrestant(type, groupe);
          console.log("Recuperation des blocs restant de cet UE ", E);
          var EE = dump.getnbblocmax(type);
          console.log("EE: ", EE);
          var blocposed = repeat_event_import(
            calendar,
            A,
            copiedDate,
            EE,
            vac,
            bsup,
            binf
          );
          blocposed = blocposed - x;
          if (E > 0) {
            // Si il reste des blocs effectivement a poser
            for (let a = 0; a < blocposed; a++) {
              console.count();
              dump.ajoutbloc(type, groupe);
            }
            /// fixing pour l'instant du soucis - savetype
            if (sessionStorage["Fixing"] == "true") {
              for (let a = 0; a < x; a++) {
                console.log("one other call to this");
                //   dump.ajoutbloc(type, groupe);
              }
            }
            /// finxing pour l'instant du soucis
          }
          var w = dump.gethposed(); // recup le tableau des heures posé
          var y = JSON.stringify(w); //stringify de ce tableau
          sessionStorage.setItem(code, y);
        }
      }
      /////////////////////////////////////////////////////////////
      else
        for (var idgroupe in liGroupe) {
          var groupe = liGroupe[idgroupe];
          console.log(
            "on est donc pas sur un CM mais sur un : ",
            type,
            "de groupe ",
            groupe
          );
          var x = cptblocidentique(liste_droped_imported, code, type, groupe);
          console.log(
            "voici le nbr de bloc identique caractérisé par ",
            code,
            type,
            groupe,
            " = ",
            x
          ); // Pourquoi 1 ?
          if (x > 0) {
            var datemoved = date;
            console.log("voici la date avant deplacement : ", datemoved);
            var A = getblocidentique(liste_droped_imported, code, type, groupe);
            console.log("On trie A");
            A.sort(compare);
            console.log(
              "Liste des blocs identique a des horaires differentes : ",
              A
            );
            var B = A[0];
            console.log(
              "Le premier element de la liste des blocs identiques :",
              B
            );
            var C = B.extendedProps.CodeUE;
            console.log("Le code UE de cet element : ", C);
            //var D = A[0].extendedProps.UE;
            var D = LiUE.find((e) => e.CodeUE == C);
            console.log(
              "Accès a l'objet UE via l'attribut UE transmis au crénaux : ",
              D
            );
            var dump = new UE(
              code,
              "newUE",
              D.effectif,
              D.CM,
              D.TD,
              D.TP,
              liGroupe,
              D.Color
            );
            console.log(
              "Generation d'un UE identique au precedent dont les méthodes sont utilisable :",
              dump
            );
            var E = dump.getblocrestant(type, groupe);
            console.log("Recuperation des blocs restant de cet UE ", E);
            var EE = dump.getnbblocmax(type);
            var blocposed = repeat_event_import(
              calendar,
              A,
              copiedDate,
              EE,
              vac,
              bsup,
              binf
            );
            blocposed = blocposed - x; // je retire ce qui a déja été posé en amont;
            if (E > 0) {
              for (let a = 0; a < blocposed; a++) {
                console.count(); //ajout des bloc effectivement posé
                dump.ajoutbloc(type, groupe);
              }

              // fixing pour l'instant le soucis de update savetype
              if (sessionStorage["Fixing"] == "true") {
                for (let a = 0; a < x; a++) {
                  console.log("one other call to this");
                  //  dump.ajoutbloc(type, groupe);
                }
              }
            }
            /// finxing pour l'instant du soucis

            var w = dump.gethposed(); // recup le tableau des heures posé
            var y = JSON.stringify(w); //stringify de ce tableau
            sessionStorage.setItem(code, y); //stockage de ce tableau qui va etre utilisé pour regenerer l'UE
          }
        }
    }
  });
}
