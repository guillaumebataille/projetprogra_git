let express = require("express");

let app = express();

let session = require("express-session");

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

//let recupJSON = require('./public/js/jsontodiv');

//let exp = require('./json/index3');

var ex = require("./json/index3");
var imp = require("./json/Import");
//var ue = require('./public/js/UE');
//var fs = require('fs');
const { waitForDebugger } = require("inspector");
//var x = require ('./public/json/index3.mjs');
//console.log(ex.hello);
let LiCren;
let LiFer;
var binf; // new
var bsup; //new
var vac = []; //new
var UEfi; //new

//console.log(ue.recupJSON());

//param = require ("./public/js/param");

//let param = require('./param.js');

//console.log(param.view());

//Moteur de template
app.set("view engine", "ejs");

//Middleware
app.use("/assets", express.static("public"));
app.use(express.static("public"));

app.use("/js", express.static(__dirname + "public/js"));
app.use("/css", express.static(__dirname + "public/css"));
app.use("/json", express.static(__dirname + "public/json"));

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(
  session({
    secret: "yoyo",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false },
  })
);

app.use(require("./middlewares/flash"));

//Routes
app.get("/", (request, response) => {
  //response.render('pages/test3', {LiUE : recupJSON()});
  response.render("pages/form", {
    link: "test3.ejs",
    page: "Semainier - TYPE",
  }); //TEST FOR UEDYNA
  //response.render('pages/test2',{ link: "test3.ejs", page: "Semainier - TYPE" });
});

app.get("/test3.ejs", (request, response) => {
  response.render("pages/test3", {
    link: "test2.ejs",
    page: "Semainier - MALEABLE",
    binf: binf,
    bsup: bsup,
    vac: [vac],
    UEfi: UEfi,
  });
});

app.get("/test2.ejs", (request, response) => {
  //response.render('pages/test3', {LiUE : recupJSON()});
  response.render("pages/test2", {
    link: "test3.ejs",
    page: "Semainier - TYPE",
  });
});

app.post("/verif.ejs", (request, response) => {
  //response.render('pages/test3', {LiUE : recupJSON()});
  console.log(request.body);
  binf = request.body.binf;
  bsup = request.body.bsup;
  vac = request.body.vac;
  response.render("pages/test2", {
    link: "test3.ejs",
    page: "Semainier - TYPE",
  });
});

app.get("/test4.ejs", (request, response) => {
  //response.render('pages/test3', {LiUE : recupJSON()});
  response.render("pages/test3", {
    link: "test2.ejs",
    page: "Semainier - MALEABLE",
  });
  console.log(request.body.essai);
});

app.post("/", (req, res) => {
  console.log("Working");
  //console.log(req.body);
  binf = req.body.borneinf; //recup borne inf
  bsup = req.body.bornesup; //recup borne sup
  vac = req.body.vac;
  UEfi = req.body.UEfilter;
  console.log(binf);
  var UEfiltred = imp.filtreUE(UEfi);
  //console.log(UEfiltred);
  //    if (UEfiltred.length == 0){
  //res.redirect('/');
  //}
  //else{
  res.render("pages/verif", { binf: binf, bsup: bsup, vac: vac });
  // }
  //fs.writeFileSync("./public/json/importUE.json",JSON.stringify(UEfiltred), null,2);
  //res.redirect('/test2.ejs');
  //res.redirect('/test.', { test : "Vous avec actueellement"});

  //res.render('pages/test2');
});

app.post("/listecrenaux", (req, res) => {
  // you have address available in req.body:

  LiCren = JSON.parse(req.body.listecrenaux);
  LiFer = JSON.parse(req.body.listeferie);
  ex.generateExcel(LiCren, LiFer, binf, bsup, vac);
  // appel function export dynamique
  console.log("Successful export");
  // always send a response:
  //console.log('Voici la liste des crenaux', LiCren);
  //console.log('Voici la liste des jours ferié', LiFer);
  res.json({ ok: true });
});

//let excel = require('./json/index3')
//let f= require('./function/function');

//console.log(f.JoursFeries(2021));

//let cal = require('@fullcalendar/core');

app.listen(8080);
