console.log("In index3.js");
//import { utils, write, writeFile } from 'xlsx';
let xlsx = require ('xlsx');
//let writeFile = require ('xlsx');
let XLSX = require ('sheetjs-style');
//import * as XLSX from 'sheetjs-style';
//import * as Excel from 'exceljs';

let recupJSON = require('./jsontodiv');

export function ColonneAD(ws) {
    ws['A1'] = { t: 't', v: "Mention :" };
    ws['A2'] = { t: 't', v: "Parcours :" };
    ws['A3'] = { t: 't', v: "Code UE :" };
    ws['A4'] = { t: 't', v: "Intitulé :" };
    ws['A4'] = { t: 't', v: "Intitulé :" };
    ws['D1'] = { t: 't', v: "A renseigner" };
    ws['D2'] = { t: 't', v: "A renseigner" };
    var indice = 1;
    for (var i =1; i<=4; i++) {
        ws['A' + indice].s = {
            font: {
                name : "Arial",
                sz: 14,
                bold: true,
                underline: true
            },
            alignment: {
                vertical: "center",
                wrapText: '1', 
            }
        };
        ws['D' + indice].s = { 
            font: {
                name : "Arial",
                sz: 14,
                bold: true,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
            }
        };
        indice++;
    }
}
export function ColonneAD2(info, ws) {
    var indice = 7;
    for (var i in info) {
        ws['A' + indice] = { t: 't', v: i }; //Du text dans la cellule
        ws['D' + indice] = { t: 't', v: info[i] };
        ws['A' + indice].s = { // set the style for target cell
            font: {
                name : "Arial",
                sz: 14,
                bold: true,
                underline: true
            },
            alignment: {
                vertical: "center",
                wrapText: '1', 
            }
        };
        ws['D' + indice].s = { 
            font: {
                name : "Arial",
                sz: 14,
                bold: true,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
                wrapText: '1', 
            }
        };
        indice++;
    }
}
export function ColonneAD3(info, ws) {
    var indice = 10;
    for (var i in info) {
        ws['A' + indice] = { t: 't', v: i }; //Du text dans la cellule
        ws['D' + indice] = { t: 't', v: info[i] };
        ws['A' + indice].s = { // set the style for target cell
            font: {
                name : "Arial",
                sz: 14,
                bold: true,
                underline: true
            },
            alignment: {
                vertical: "center",
                wrapText: '1', 
            },
        };
        ws['D' + indice].s = {
            font: {
                name : "Arial",
                sz: 14,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
                //horizontal: "center",
                wrapText: '1', // any truthy value here
            }
        };
        indice++;
    }
}

export function ColonneDE(ws) {
    ws['D10'] = { t: 't', v: " Initiales " };
    ws['E10'] = { t: 't', v: " Nom Prénom " };
    ws['D10'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "thin",
                color: "000000"
            },
            left: {
                style: "medium",
                color: "000000"
            },
            top: {
                style: "medium",
                color: "000000"
            },
            bottom: {
                style: "thin",
                color: "000000"
            }
        }
    };
    ws['E10'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "thin",
                color: "000000"
            },
            top: {
                style: "medium",
                color: "000000"

            },
            bottom: {
                style: "thin",
                color: "000000"
            }
        }
    };

    for (var indice = 11; indice <= 15; indice++) {
        ws['D' + indice].s = {
            font: {
                name: "Verdana",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "thin",
                    color: "000000"
                },
                left: {
                    style: "medium",
                    color: "000000"
                },
                top: {
                    style: "thin",
                    color: "000000"
                },
                bottom: {
                    style: "thin",
                    color: "000000"
                }
            }
        };
        ws['E' + indice].s = {
            font: {
                name: "Verdana",
                sz: 12,
                bold: true
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "medium",
                    color: "000000"
                },
                left: {
                    style: "thin",
                    color: "000000"
                },
                top: {
                    style: "thin",
                    color: "000000"

                },
                bottom: {
                    style: "thin",
                    color: "000000"
                }
            }
        };
    }
    ws['D16'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "thin",
                color: "000000"
            },
            left: {
                style: "medium",
                color: "000000"
            },
            top: {
                style: "thin",
                color: "000000"
            },
            bottom: {
                style: "medium",
                color: "000000"
            }
        }
    };
    ws['E16'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "thin",
                color: "000000"
            },
            top: {
                style: "thin",
                color: "000000"

            },
            bottom: {
                style: "medium",
                color: "000000"
            }
        }
    };
}
export function ColonneIV(ws) {
    ws['I10'] = { t: 't', v: "Indiquer les créneaux par un X si une salle à réserver par planning " }; //Du text dans la cellule
    ws['I11'] = { t: 't', v: "Indiquer les créneaux par un A si salle hors FDS, préciser la salle à indiquer en \"notes\" " };
    ws['I12'] = { t: 't', v: "Indiquer les créneaux par un I si salle informatisée" };
    ws['R11'] = { t: 't', v: "+ initiales enseignants si différent à chaque séance" }; //Du text dans la cellule
    ws['I10'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "Arial",
            sz: 14,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K10'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L10'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['Q10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['I11'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "Arial",
            sz: 14,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['Q11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['I12'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K12'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L12'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['Q12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['R11'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "arial",
            sz: 14,
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['S11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['T11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['U11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['V11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['W11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['X11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
}
export function ColonneQV11(ws) {
    for (var indice = 10; indice <= 13; indice += 2) {
        ws['Q' + indice] = { t: 't', v: "+ initiales enseignants si différent à chaque séance" }; //Du text dans la cellule          
        ws['Q' + indice].s = { // set the style for target cell     
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            },
            font: {
                name: "Arial",
                sz: 14,
                //bold : true, 
            },
            alignment: {
                vertical: "center",
            }
        };
        ws['R' + indice].s = { // definir le fond en vert 
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['S' + indice].s = { // definir le fond en vert     
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['T' + indice].s = { // definir le fond en vert     
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['U' + indice].s = { // definir le fond en vert 
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['V' + indice].s = { // definir le fond en vert 
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['W' + indice].s = { // definir le fond en vert 
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['X' + indice].s = { // definir le fond en vert 
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
    }
}
export function TableauMT(ws) {
    ws['Q2'] = { t: 't', v: " CM" }; //Du text dans la cellule
    ws['R2'] = { t: 't', v: " TD" };
    ws['S2'] = { t: 't', v: " TP" };
    ws['T2'] = { t: 't', v: " TERRAIN" };
    ws['R6'] = { t: 't', v: " sur la base des prévisions maquettes (fichiers jaunes)" };

    ws['O3'] = { t: 't', v: " CHARGES : " }; //Du text dans la cellule
    ws['N4'] = { t: 't', v: " Multiple de 1h30 : " };
    ws['N5'] = { t: 't', v: " Multiple de 3h : " };
    ws['O6'] = { t: 't', v: " Effectif previsionnel 21-22 :" };

    ws['R6'].s = {
        font: {
            name: "arial",
            sz: 12,
            italic: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "left"
        },

    };
    ws['Q2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['R2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['S2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['T2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['O3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['N3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }
    };
    ws['M3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N4'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            italic: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }
    };
    ws['M4'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['M5'].s = { // set the style for target cell
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N5'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            italic: true,
            sz: 14,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            bottom: {
                style: "thin"
            }
        }
    };
    ws['O6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 13,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            }
        }
    };
    ws['M6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            }
        }
    };
    ws['O4'].s = { // set the style for target cell
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            }
        }
    };
    ws['O5'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            right: {
                style: "thin"
            }
        }
    };
    ws['Q3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['Q5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['Q6'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };

    ws['R4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['R5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['S3'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['S5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T3'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            right: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }

    ws['P2'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['P4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['P6'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['M2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['N2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['O2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
}

export function ColonneEH7(ws) {
    ws['E7'] = { t: 't', v: "          Si OUI indiquer les parcours :" };
    ws['G7'] = { t: 't', v: "A renseigner" };
    ws['E7'].s = {  
        font: {
            name: "Arial",
            sz: 14,
            bold: true,
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['G7'].s = {
        font: {
            name: "Arial",
            sz: 14,
            bold: true,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
}

export function ColonneCM(ws) {
    ws['C19'] = { t: 't', v: "        CM    " };
    ws['C19'].s = {
        font: {
            name: "Arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
export function ColonneTD(ws,j) {
    ws['C'+j] = { t: 't', v: "        TD    " };
    ws['C'+j].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
export function boucleForYellow(ws) {

    ws['H19'] = { t: 't', v: "S1" };
    ws['I19'] = { t: 't', v: "S2" };
    ws['J19'] = { t: 't', v: "S3" };
    ws['K19'] = { t: 't', v: "S4" };
    ws['L19'] = { t: 't', v: "S5" };
    ws['M19'] = { t: 't', v: "S6" };
    ws['N19'] = { t: 't', v: "S7" };
    ws['O19'] = { t: 't', v: "S8" };
    ws['Q19'] = { t: 't', v: "S10" };
    ws['R19'] = { t: 't', v: "S11" };
    ws['S19'] = { t: 't', v: "S12" };
    ws['T19'] = { t: 't', v: "S13" };
    ws['U19'] = { t: 't', v: "S14" };
    ws['V19'] = { t: 't', v: "S15" };
    ws['W19'] = { t: 't', v: "S16" };
    ws['X19'] = { t: 't', v: "S17" };
    ws['A20'] = { t: 't', v: "Jour" };
    ws['B20'] = { t: 't', v: "Créneau" };
    ws['C20'] = { t: 't', v: "Créneau non classique" };
    ws['D20'] = { t: 't', v: "Enseignant" };
    ws['E20'] = { t: 't', v: "Groupe/Série " };
    ws['F20'] = { t: 't', v: "Effectif" };
    ws['G20'] = { t: 't', v: "Salle" };
    ws['P19'] = { t: 't', v: "VACANCES FEVRIER" };
    ws['J20'] = { t: 't', v: "  1  " };
    ws['K20'] = { t: 't', v: "  2  " };
    ws['L20'] = { t: 't', v: "  3  " };
    ws['M20'] = { t: 't', v: "  4  " };
    ws['N20'] = { t: 't', v: "  5  " };
    ws['O20'] = { t: 't', v: "  6  " };
    ws['Q20'] = { t: 't', v: "  7  " };
    ws['R20'] = { t: 't', v: "  8  " };
    ws['S20'] = { t: 't', v: "  9  " };
    ws['T20'] = { t: 't', v: "  10  " };
    ws['U20'] = { t: 't', v: "  11  " };
    ws['V20'] = { t: 't', v: "  12  " };
    ws['W20'] = { t: 't', v: "  13  " };
    ws['X20'] = { t: 't', v: "  14  " };
    ws['I18'] = { t: 't', v: "Session 1 pairs Licence et Master" };
    ws['J18'] = { t: 't', v: "Lundi 17 CM UNIQUEMENT" };
    ws['T18'] = { t: 't', v: "Session 2 impairs Master  - Pas de CM >80 étudiants" };
    ws['W18'] = { t: 't', v: "lundi 18 férié" };


    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }

    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '20'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '20'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }

    
    for (var indice = 'H'.charCodeAt(0); indice <= 'J'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '18'].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    ws['T18'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


    ws['W18'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


}
export function BoucleTD2(ws, j) {

    ws['H'+j] = { t: 't', v: "S1" };
    ws['I'+j] = { t: 't', v: "S2" };
    ws['J'+j] = { t: 't', v: "S3" };
    ws['K'+j] = { t: 't', v: "S4" };
    ws['L'+j] = { t: 't', v: "S5" };
    ws['M'+j] = { t: 't', v: "S6" };
    ws['N'+j] = { t: 't', v: "S7" };
    ws['O'+j] = { t: 't', v: "S8" };
    ws['Q'+j] = { t: 't', v: "S10" };
    ws['R'+j] = { t: 't', v: "S11" };
    ws['S'+j] = { t: 't', v: "S12" };
    ws['T'+j] = { t: 't', v: "S13" };
    ws['U'+j] = { t: 't', v: "S14" };
    ws['V'+j] = { t: 't', v: "S15" };
    ws['W'+j] = { t: 't', v: "S16" };
    ws['X'+j] = { t: 't', v: "S17" };
    ws['A'+(j+1)] = { t: 't', v: "Jour" };
    ws['B'+(j+1)] = { t: 't', v: "Créneau" };
    ws['C'+(j+1)] = { t: 't', v: "Créneau non classique" };
    ws['D'+(j+1)] = { t: 't', v: "Enseignant" };
    ws['E'+(j+1)] = { t: 't', v: "Groupe/Série " };
    ws['F'+(j+1)] = { t: 't', v: "Effectif" };
    ws['G'+(j+1)] = { t: 't', v: "Salle" };
    ws['P'+j] = { t: 't', v: "VACANCES FEVRIER" };
    ws['J'+(j+1)] = { t: 't', v: "  1  " };
    ws['K'+(j+1)] = { t: 't', v: "  2  " };
    ws['L'+(j+1)] = { t: 't', v: "  3  " };
    ws['M'+(j+1)] = { t: 't', v: "  4  " };
    ws['N'+(j+1)] = { t: 't', v: "  5  " };
    ws['O'+(j+1)] = { t: 't', v: "  6  " };
    ws['Q'+(j+1)] = { t: 't', v: "  7  " };
    ws['R'+(j+1)] = { t: 't', v: "  8  " };
    ws['S'+(j+1)] = { t: 't', v: "  9  " };
    ws['T'+(j+1)] = { t: 't', v: "  10  " };
    ws['U'+(j+1)] = { t: 't', v: "  11  " };
    ws['V'+(j+1)] = { t: 't', v: "  12  " };
    ws['W'+(j+1)] = { t: 't', v: "  13  " };
    ws['X'+(j+1)] = { t: 't', v: "  14  " };
    ws['I'+(j-1)] = { t: 't', v: "Session 1 pairs Licence et Master" };
    ws['J'+(j-1)] = { t: 't', v: "Lundi 17 CM UNIQUEMENT" };
    ws['T'+(j-1)] = { t: 't', v: "Session 2 impairs Master  - Pas de CM >80 étudiants" };
    ws['W'+(j-1)] = { t: 't', v: "lundi 18 férié" };
    ws['Z18'] = { t: 't', v: "Session 1     semestres pairs    Du 10 au 20 Mai" };
    ws['Z25'] = { t: 't', v: "Session 1     semestres pairs    Du 10 au 20 Mai" };
    ws['Z39'] = { t: 't', v: "Session 1     semestres pairs    Du 10 au 20 Mai" };
    ws['Z32'] = { t: 't', v: "Session 1     semestres pairs    Du 10 au 20 Mai" };


    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + j].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + j].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + j].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (j+1)].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }

    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (j+1)].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }

    for (var indice = 18; indice <= 44; indice++) {
        ws['Z' + indice].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }

    for (var indice = 'H'.charCodeAt(0); indice <= 'J'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre +  + (j-1)].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    ws['T'+(j-1)].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


    ws['W'+(j-1)].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


}
export function ColonneTP(ws, l) {
    ws['C'+l] = { t: 't', v: "        TP    " };
    ws['C'+l].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
export function ColonneTERRAIN(ws, m) {
    ws['C'+m] = { t: 't', v: "        TERRAIN    " };
    ws['C'+m].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
export function boucleTP2(ws, l) {

    ws['H'+l] = { t: 't', v: "S1" };
    ws['I'+l] = { t: 't', v: "S2" };
    ws['J'+l] = { t: 't', v: "S3" };
    ws['K'+l] = { t: 't', v: "S4" };
    ws['L'+l] = { t: 't', v: "S5" };
    ws['M'+l] = { t: 't', v: "S6" };
    ws['N'+l] = { t: 't', v: "S7" };
    ws['O'+l] = { t: 't', v: "S8" };
    ws['Q'+l] = { t: 't', v: "S10" };
    ws['R'+l] = { t: 't', v: "S11" };
    ws['S'+l] = { t: 't', v: "S12" };
    ws['T'+l] = { t: 't', v: "S13" };
    ws['U'+l] = { t: 't', v: "S14" };
    ws['V'+l] = { t: 't', v: "S15" };
    ws['W'+l] = { t: 't', v: "S16" };
    ws['X'+l] = { t: 't', v: "S17" };
    ws['A'+(l+1)] = { t: 't', v: "Jour" };
    ws['B'+(l+1)] = { t: 't', v: "Créneau" };
    ws['C'+(l+1)] = { t: 't', v: "Créneau non classique" };
    ws['D'+(l+1)] = { t: 't', v: "Enseignant" };
    ws['E'+(l+1)] = { t: 't', v: "Groupe/Série " };
    ws['F'+(l+1)] = { t: 't', v: "Effectif" };
    ws['G'+(l+1)] = { t: 't', v: "Salle" };
    ws['P'+l] = { t: 't', v: "VACANCES FEVRIER" };
    ws['J'+(l+1)] = { t: 't', v: "  1  " };
    ws['K'+(l+1)] = { t: 't', v: "  2  " };
    ws['L'+(l+1)] = { t: 't', v: "  3  " };
    ws['M'+(l+1)] = { t: 't', v: "  4  " };
    ws['N'+(l+1)] = { t: 't', v: "  5  " };
    ws['O'+(l+1)] = { t: 't', v: "  6  " };
    ws['Q'+(l+1)] = { t: 't', v: "  7  " };
    ws['R'+(l+1)] = { t: 't', v: "  8  " };
    ws['S'+(l+1)] = { t: 't', v: "  9  " };
    ws['T'+(l+1)] = { t: 't', v: "  10  " };
    ws['U'+(l+1)] = { t: 't', v: "  11  " };
    ws['V'+(l+1)] = { t: 't', v: "  12  " };
    ws['W'+(l+1)] = { t: 't', v: "  13  " };
    ws['X'+(l+1)] = { t: 't', v: "  14  " };
    ws['I'+(l-1)] = { t: 't', v: "Session 1 pairs Licence et Master" };
    ws['J'+(l-1)] = { t: 't', v: "Lundi 17 CM UNIQUEMENT" };
    ws['T'+(l-1)] = { t: 't', v: "Session 2 impairs Master  - Pas de CM >80 étudiants" };
    ws['W'+(l-1)] = { t: 't', v: "lundi 18 férié" };


    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + l].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + l].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + l].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre +(l+1)].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre +(l+1)].s = {
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }

    for (var indice = 'H'.charCodeAt(0); indice <= 'J'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre +(l-1)].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    ws['T' +(l-1)].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


    ws['W' +(l-1)].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


}
export function boucleTERRAIN2(ws, m) {

    ws['H'+m] = { t: 't', v: "S1" };
    ws['I'+m] = { t: 't', v: "S2" };
    ws['J'+m] = { t: 't', v: "S3" };
    ws['K'+m] = { t: 't', v: "S4" };
    ws['L'+m] = { t: 't', v: "S5" };
    ws['M'+m] = { t: 't', v: "S6" };
    ws['N'+m] = { t: 't', v: "S7" };
    ws['O'+m] = { t: 't', v: "S8" };
    ws['Q'+m] = { t: 't', v: "S10" };
    ws['R'+m] = { t: 't', v: "S11" };
    ws['S'+m] = { t: 't', v: "S12" };
    ws['T'+m] = { t: 't', v: "S13" };
    ws['U'+m] = { t: 't', v: "S14" };
    ws['V'+m] = { t: 't', v: "S15" };
    ws['W'+m] = { t: 't', v: "S16" };
    ws['X'+m] = { t: 't', v: "S17" };
    ws['A'+(m+1)] = { t: 't', v: "Jour" };
    ws['B'+(m+1)] = { t: 't', v: "Créneau" };
    ws['C'+(m+1)] = { t: 't', v: "Créneau non classique" };
    ws['D'+(m+1)] = { t: 't', v: "Enseignant" };
    ws['E'+(m+1)] = { t: 't', v: "Groupe/Série " };
    ws['F'+(m+1)] = { t: 't', v: "Effectif" };
    ws['G'+(m+1)] = { t: 't', v: "Salle" };
    ws['P'+m] = { t: 't', v: "VACANCES FEVRIER" };
    ws['J'+(m+1)] = { t: 't', v: "  1  " };
    ws['K'+(m+1)] = { t: 't', v: "  2  " };
    ws['L'+(m+1)] = { t: 't', v: "  3  " };
    ws['M'+(m+1)] = { t: 't', v: "  4  " };
    ws['N'+(m+1)] = { t: 't', v: "  5  " };
    ws['O'+(m+1)] = { t: 't', v: "  6  " };
    ws['Q'+(m+1)] = { t: 't', v: "  7  " };
    ws['R'+(m+1)] = { t: 't', v: "  8  " };
    ws['S'+(m+1)] = { t: 't', v: "  9  " };
    ws['T'+(m+1)] = { t: 't', v: "  10  " };
    ws['U'+(m+1)] = { t: 't', v: "  11  " };
    ws['V'+(m+1)] = { t: 't', v: "  12  " };
    ws['W'+(m+1)] = { t: 't', v: "  13  " };
    ws['X'+(m+1)] = { t: 't', v: "  14  " };
    ws['I'+(m-1)] = { t: 't', v: "Session 1 pairs Licence et Master" };
    ws['J'+(m-1)] = { t: 't', v: "Lundi 17 CM UNIQUEMENT" };
    ws['T'+(m-1)] = { t: 't', v: "Session 2 impairs Master  - Pas de CM >80 étudiants" };
    ws['W'+(m-1)] = { t: 't', v: "lundi 18 férié" };


    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + m].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + m].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + m].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (m+1)].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre +(m+2)].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (m+1)].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (m+2)].s = {

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (m+3)].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }

    for (var indice = 'H'.charCodeAt(0); indice <= 'J'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (m-1)].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    ws['T'+(m-1)].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


    ws['W'+(m-1)].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


}
export function QRS(ws){

    for (var indice = 'Q'.charCodeAt(0); indice < 'T'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
    ws[lettre + '3'].s = {
        font: {
            name : "arial",
            sz: 14,
            bold : true,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            },
        border: {
            bottom: {
                style: "thin"
                },
            right: {
                style: "thin"
                },
            left: {
                style: "thin"
                }
            }
         };
         ws[lettre + '4'].s = {
            font: {
                name : "arial",
                sz: 14,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                },
            border: {
                bottom: {
                    style: "thin"
                    },
                right: {
                    style: "thin"
                    },
                left: {
                    style: "thin"
                    }
                }
             };
    }
    ws['S5'].s = {
        font: {
            name : "arial",
            sz: 14,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            },
        border: {
            bottom: {
                style: "thin"
                },
            right: {
                style: "thin"
                },
            left: {
                style: "thin"
                }
            }
         };
         ws['Q6'].s = {
            font: {
                name : "arial",
                sz: 14,
                bold : true,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                },
            border: {
                bottom: {
                    style: "thin"
                    },
                right: {
                    style: "thin"
                    },
                left: {
                    style: "thin"
                    }
                }
             };
}

/*export function ColonneAC(ws){
    ws['A1'] = { t: 't', v: " Code UE " };
    ws['B1'] = { t: 't', v: " Nom UE" };
    ws['A1'].s = {   
        fill: {
            fgColor: { rgb: "FFFFF00" }
        },
        font: {
          name : "Verdana",
          sz: 12,
          bold : true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "medium",
                color: "000000"
            },
            top :{
                style : "medium",
                color: "000000"
            },
            bottom :{
                style : "medium",
                color: "000000"
            }
        }
      };
    ws['B1'].s = {
        fill: {
            fgColor: { rgb: "FFFFF00" }
        },
        font: {
          name : "Verdana",  
          sz: 12,
          bold : true
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "thin",
                color: "000000"
            },
            top :{
                style : "medium",
                color: "000000"

            },
            bottom :{
                style : "medium",
                color: "000000"
            }
        }
      };
    var contenuJSON = recupJSON();
    var y = []; 
    var x = [];
    contenuJSON.forEach(Objet => {
        y.push(Objet['Code UE']);
      });
    contenuJSON.forEach(Objet => {
        x.push(Objet['Libellé']);
      });
    for (var indice=0;indice<y.length;indice++){
        ws['A'+(indice+2)]= {t: 't', v: y[indice]};
        ws['B'+(indice+2)]= {t: 't', v: x[indice]};
      
        ws['A' + (indice+2)].s = {   
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
              name : "Verdana",
              sz: 12,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "medium",
                    color: "000000"
                },
                left: {
                    style: "medium",
                    color: "000000"
                },
                top :{
                    style : "medium",
                    color: "000000"
                },
                bottom :{
                    style : "medium",
                    color: "000000"
                }
            }
          };
        ws['B' + (indice+2)].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
              name : "Verdana",  
              sz: 12,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "medium",
                    color: "000000"
                },
                top :{
                    style : "medium",
                    color: "000000"

                },
                bottom :{
                    style : "medium",
                    color: "000000"
                }
            }
          };
        }
}*/

export function filtreUE(codeUE, liste) {
	var res = [];
	liste.forEach(el => {
		if (el.extendedProps.CodeUE == codeUE) {
			res.push(el);
		}
	});
    //console.log("Voici le filtreUE:", res);
    return res;
};

export function filtretype(type, liste) {
	var res = [];
	liste.forEach(el => {
		if (el.extendedProps.Type == type) {
			res.push(el);
		}
	});
    //console.log("Voici le filtre TP:", res);
	return res;
};
export function getJour(crenaux) {
	x = new Date(crenaux.start).getDay();
    //console.log("auj:", x.getDay());
	switch (x) {
		case (1):
			return "Lundi";
			break;
		case (2):
			return "Mardi";
			break;
		case (3):
			return "Mercredi";
			break;
		case (4):
			return "Jeudi";
			break;
		case (5):
			return "Vendredi";
			break;
	}
};
export function pad(number) {
	if (number < 10) {
		return '0' + number;
	}
	return number;
};

export function getdure(crenaux) {
	ah = pad(new Date(crenaux.start).getHours()).toString();
	am = pad(new Date(crenaux.start).getMinutes()).toString();
	bh = pad(new Date(crenaux.end).getHours()).toString();
	bm = pad(new Date(crenaux.end).getMinutes()).toString();
	return ah + "h" + am + "-" + bh + "h" + bm;
};

export function format_DJG(crenaux) {
	return [getJour(crenaux), getdure(crenaux), crenaux.extendedProps.Groupe];

}

export function convertall_DJG(liste) {
	var x = [];
	liste.forEach(el => {
		var y = 0;
		var z = format_DJG(el);
		x.forEach(ele => {
			if (ele[0] == z[0]) {
				if (ele[1] == z[1]) {
					if (ele[2] == z[2]) {
						y = 1;
					}
				}
			}
		});
		if (y == 0) {
			x.push(format_DJG(el));
		};
	});
	return x;
};