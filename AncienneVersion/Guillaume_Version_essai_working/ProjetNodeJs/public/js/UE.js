
console.log('In UE.js');
//Création de la classe UE
class UE {
  constructor(code, nom, nbCM, nbTD, nbTP, liGroupe, x, Color) {
    this.CodeUE = code;
    this.Nom = nom;
    this.CM = nbCM;
    this.TD = nbTD;
    this.TP = nbTP;
    this.color = Color;
    this.ListeGroupe = [];
    //this.Creneaux = [];
    for (var i = 0; i < liGroupe.length; i++) { //pour nbr de groupe associé a l'UE faire
      var t = liGroupe[i]; // on stocke le nom du groupe i
      this.ListeGroupe[i] = t;
    }
    //console.log("init value = = ", sessionStorage['init']);
    if (sessionStorage[code] != undefined) {
      this.H_posed = JSON.parse(sessionStorage[code]);
    }
    else {
  //    console.log('constructing a new H_posed');
      this.H_posed = [];
      for (var i = 0; i < liGroupe.length; i++) {
        this.H_posed.push([0, 0, 0]);
      }
    }
  }
  getNom() {
    return this.Nom;
  }
  gethposed() {
    return this.H_posed;
  }
  //Ajoute une CM à tous les groupes affectés à cette UE
  ajoutCM() {
    var x = get_x_format();
    for (var i = 0; i < this.ListeGroupe.length; i++) {
      //this.Creneaux[this.ListeGroupe[i]]["CM"].push(date);
      //J'y ajoute 1 si on vient de poser 1 bloc d'une heure ou 1,5 sinon.
      this.H_posed[i][0] += x;
      //console.log(this.H_posed[this.ListeGroupe[i]]["CM"]);
    }
  }
  //Ajoute un CM à un groupe particulier
  ajoutCMGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][0] += x;
  }
  //Ajoute un TD à un groupe particulier
  ajoutTDGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][1] += x;
  }
  //Ajoute un TP à un groupe particulier
  ajoutTPGroupe(groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    this.H_posed[i][2] += x
  }
  ajoutbloc(type, groupe) {
    if (type == 'CM') {
      return this.ajoutCM();
    } else if (type == 'TD') {
      return this.ajoutTDGroupe(groupe);
    } else if (type == 'TP') {
      return this.ajoutTPGroupe(groupe);
    }
  }
  CMrestant() {
    return this.CM;
  }
  getblocrestant(type, groupe) {
    var x = get_x_format();
    var i = this.ListeGroupe.indexOf(groupe);
    if (type == 'CM') {
      return this.CM / x - this.H_posed[i][0] / x; // [0] est donc ici arbitraire
    }
    else if (type == 'TD') {
      return this.TD / x - this.H_posed[i][1] / x;
    }
    else if (type == 'TP') {
      return this.TP / x - this.H_posed[i][2] / x;
    }
  }
  getnbblocmax(type) {

    var x = get_x_format();

    if (type == 'CM') {
      return this.CM / x;
    }
    else if (type == 'TD') {
      return this.TD / x;
    }
    else if (type == 'TP') {
      return this.TP / x;
    }
  }
}


var liGroupe = ["A", "B", "C", "D"];

var liCodeUe = new Object();

function recupJSON() {
  //console.log("Enter recupJSON(): ");
  $.ajaxSetup({
    async: false
});

  var LiUE = new Object;
  var liCodeUe = new Object;
  $.getJSON('json/test3.json', function (data) {
    $.each(data, (_index, objet) => {
      LiUE[objet.CodeUE] = new UE(objet.CodeUE, objet.NomUE, objet.CM, objet.TD, objet.TP, liGroupe, sessionStorage["init"],objet.C);
      liCodeUe[objet.CodeUE] = objet.CodeUE;
      //console.log("Creation de LiUE :", LiUE);
      //console.log("Creation de liCodeUe :", liCodeUe);
    })
    $.ajaxSetup({
      async: true
  });
  
    if (sessionStorage['init'] != 'true') {
      sessionStorage.setItem("init", "true");
    }
  })
  return LiUE
};

function recupCode() {
//  console.log("Enter recupJSON(): ");
  $.ajaxSetup({
    async: false
});

  var LiUE = new Object;
  var liCodeUe = new Object;
  $.getJSON('json/test3.json', function (data) {
    $.each(data, (_index, objet) => {
      LiUE[objet.CodeUE] = new UE(objet.CodeUE, objet.NomUE, objet.CM, objet.TD, objet.TP, liGroupe, sessionStorage["init"],objet.C);
      liCodeUe[objet.CodeUE] = objet.CodeUE;
      //console.log("Creation de LiUE :", LiUE);
      //console.log("Creation de liCodeUe :", liCodeUe);
    })
    $.ajaxSetup({
      async: true
  });
  
   /* if (sessionStorage['init'] != 'true') {
      sessionStorage.setItem("init", "true");
    }*/
  })

  return liCodeUe;
};



/*function initLiUE() {
  console.log("Enter initLiUE() ");
  return new Promise((Resolve, Reject) => {
  $.getJSON("test3.json", function (data) {
    $.each(data, (_index, objet) => {
      LiUE[objet.CodeUE] = new UE(objet.CodeUE, objet.NomUE, objet.CM, objet.TD, objet.TP, liGroupe, sessionStorage["init"],objet.C);
      console.log("Generating...", LiUE);
    })
    if (sessionStorage['init'] != 'true') {
      console.log("INIT == TRUE")
      sessionStorage.setItem("init", "true");
    }
  })

  Resolve(LiUE);
})
};

var liCodeUe = new Object;
function initLiCodeUe() {
  return new Promise((Resolve, Reject) => {
    console.log("Enter initLiCodeUE() ");
    //var LiCodeUe = new Object;
    $.getJSON("test3.json", function (data) {
      $.each(data, (_index, objet) => {
        liCodeUe[objet.CodeUE] = [objet.CodeUE];
        console.log("Generating...", liCodeUe);
      })
      if (sessionStorage['init'] != 'true') {
        sessionStorage.setItem("init", "true");
      }
    })
  
    //Resolve(console.log("Done LiUE", LiUE))
    Resolve();
  });
};*/






/*function loadExtEvents() {
  console.log("Getting onto loadExtEvents : ", LiUE, liCodeUe)
  //var format = sessionStorage['format'];
  for (var i in liCodeUe) {
  console.log ("for i=",i,"in ",liCodeUe);
    document.getElementById('external-events').innerHTML +=
      '<p> <strong>' + '<div>' + LiUE[i].Nom + '</strong> </p>' +
      ' CM : ' + LiUE[i].getblocrestant("CM", "A") + '/' + LiUE[i].getnbblocmax('CM');
    LiUE[i].ListeGroupe.forEach(element => {
      document.getElementById('external-events').innerHTML +=
        "TD_" + element + " : " + LiUE[i].getblocrestant("TD", element) + '/' + LiUE[i].getnbblocmax('TD') + '</br>' +
        " TP_" + element + " : " + LiUE[i].getblocrestant("TP", element) + '/' + LiUE[i].getnbblocmax('TP') + '</br>';
    });

    document.getElementById('external-events').innerHTML += '</p>';
    if (LiUE[i].getblocrestant('CM', 'A') > 0) {
      document.getElementById('external-events').innerHTML +=
        '<div id="' + i + '" class=\'fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event\'>' +
        '<div class="fc-event-main">' +
        '<div>CM' +
        '</div>' +
        '</div>' +
        '</div>';
    }
    LiUE[i].ListeGroupe.forEach(element => {

      if (LiUE[i].getblocrestant('TD', element) > 0) {
        document.getElementById('external-events').innerHTML +=
          '<div id="' + i + '" class=\'fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event\'>' +
          '<div class="fc-event-main">' +
          '<div>TD_' +
          element +
          '</div>' +
          '</div>' +
          '</div>';
      }

      if (LiUE[i].getblocrestant('TP', element) > 0) {
        document.getElementById('external-events').innerHTML +=
          '<div id="' + i + '" class=\'fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event\'>' +
          '<div class="fc-event-main">' +
          '<div class="case-ctp">TP_' +
          element +
          '</div>' +
          '</div>' +
          '</div>';
      }
    });
  };
};


function reloadEvents() {
  document.getElementById('external-events').innerHTML = "";
  loadExtEvents();
};

async function doWork(code) {
  //console.log("Working...");
  var x = await initLiUE();// Await retourne la partie resolve response de la fonction appliquée
  //console.log(x);
  var y = await initLiCodeUe();
  //console.log(y); // le await n'arrete pas le programme en entier, il va juste stoper tout les wait et les resoudre 1 par 1
  //var z = loadExtEvents();
  //console.log(z);
return };*/
