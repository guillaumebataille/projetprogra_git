console.log("In function.js");

function JoursFeries(an) {
	var JourAn = new Date(an, "00", "01")
	var FeteTravail = new Date(an, "04", "01")
	var Victoire1945 = new Date(an, "04", "08")
	var FeteNationale = new Date(an, "06", "14")
	var Assomption = new Date(an, "07", "15")
	var Toussaint = new Date(an, "10", "01")
	var Armistice = new Date(an, "10", "11")
	var Noel = new Date(an, "11", "25")

	var G = an % 19
	var C = Math.floor(an / 100)
	var H = (C - Math.floor(C / 4) - Math.floor((8 * C + 13) / 25) + 19 * G + 15) % 30
	var I = H - Math.floor(H / 28) * (1 - Math.floor(H / 28) * Math.floor(29 / (H + 1)) * Math.floor((21 - G) / 11))
	var J = (an * 1 + Math.floor(an / 4) + I + 2 - C + Math.floor(C / 4)) % 7
	var L = I - J
	var MoisPaques = 3 + Math.floor((L + 40) / 44)
	var JourPaques = L + 28 - 31 * Math.floor(MoisPaques / 4)
	var Paques = new Date(an, MoisPaques - 1, JourPaques)
	var LundiPaques = new Date(an, MoisPaques - 1, JourPaques + 1)
	var Ascension = new Date(an, MoisPaques - 1, JourPaques + 39)
	var Pentecote = new Date(an, MoisPaques - 1, JourPaques + 49)
	var LundiPentecote = new Date(an, MoisPaques - 1, JourPaques + 50)

	return new Array(JourAn, Paques, LundiPaques, FeteTravail, Victoire1945, Ascension, Pentecote, LundiPentecote, FeteNationale, Assomption, Toussaint, Armistice, Noel)
}

//Fonction pour ajouter le 0 si necessaire
function pad(number) {
	if (number < 10) {
		return '0' + number;
	}
	return number;
};

//fonction pour generer a partir d'un jour ferie code dans le tableau "array_ferie" la date au bon format
function ferie_gen(array_ferie, i) {
	var out = toY_M_D(array_ferie[i]);
	return {
		start: out,
		end: out,
		display: 'background',
		overlap: false,
	}
};

var current_year = (new Date()).getFullYear();
var array_ferie = JoursFeries(current_year);
var array_ferie_after = JoursFeries(current_year + 1);
var array_ferie_before = JoursFeries(current_year - 1);
var all_ferie = array_ferie.concat(array_ferie_after).concat(array_ferie_before);

//fonction qui a partir d'une date, genere un date au bon format ISO : YYYY-MM-DD
function toY_M_D(item) { return item.getFullYear().toString() + "-" + pad(item.getMonth() + 1).toString() + "-" + pad(item.getDate()).toString() };

//fonction qui a partir d'une date, genere un date au bon format ISO : HH:MM:00
function toH_M_S(item) { return pad(item.getHours()).toString() + ":" + pad(item.getMinutes()).toString() + ":00" };


//fonction qui a partir d'un event, retourne true si il est sur un jour férié et false sinon.

function ferie(event) {
	var date_event = toY_M_D(event._instance.range.start); // Recup la date de l'event au bon format YYYY-MM-DD
	var res = false;
	//Parcours de toutes les dates feriée
	all_ferie.forEach(item => {
		if (toY_M_D(item) == date_event) { // Si ka date de l'event correspond a une date feriée, on passe res a true
			res = true;
		};
	})
	return res;
};

//fonction qui a partir d'une date retourne true si elle est egale a un date ferié et false sinon.

function ferie_date(date) {
	var date = toY_M_D(date);
	var res = false;
	//Parcours de toutes les dates feriée
	all_ferie.forEach(item => {
		if (toY_M_D(item) == date) { // Si ka date de l'event correspond a une date feriée, on passe res a true
			res = true;
		};
	})
	return res;
};

//fonction pour acceder simplement au codeUE via un event/crenaux
function getcodeUE(event) {
	return event._def.extendedProps.CodeUE;
};


//fonction pour acceder simplement a la date de debut
function getstart(event) {
	return event._instance.range.start;
};

//fonction pour acceder simplement a la date de fin
function getend(event) {
	return event._instance.range.end;
};

//fonction pour acceder simplement au type d'un event
function gettype(event) {
	return event._def.extendedProps.Type;
};

//fonction pour acceder simplement au type d'un event
function getprof(event) {
	return event._def.extendedProps.profR;
};

//fonction pour acceder simplement au tableau contenant le/les groupes d'un event
function getgroupe(event) {
	return event._def.extendedProps.Groupe;
};




//fonction pour a partir d'un crenaux degager l'heure debut et fin au format : hh:mm-hh:mm
function getduree(event) {
	var start = getstart(event);
	var end = getend(event);
	return pad(start.getUTCHours().toString()) + ':' + pad(start.getMinutes().toString()) + '-' + pad(end.getUTCHours().toString()) + ':' + pad(end.getMinutes().toString());
};

//fonction qui a partir d'une date retourne le lundi de la semaine de la date 
function Mon_Week() {
	var date = new Date();
	if (date.getDay() == 0) { // si on est dimanche, je reviens une semaine en arriere 
		return (new Date(date.setDate(date.getDate() - 7 + 1)));
	}
	return new Date(date.setDate(date.getDate() - date.getDay() + 1));

};


//fonction qui a partir d'une date retourne le samedi de la semaine de la date 
function Sat_Week() {
	var date = new Date();
	if (date.getDay() == 0) { // si on est dimanche, je reviens une semaine en arriere 
		return (new Date(date.setDate(date.getDate() - 7 + 6)));
	}
	return new Date(date.setDate(date.getDate() - date.getDay() + 6));
};


//fonction qui a partir d'une date retourne le vendredi de la semaine de la date 
function Wen_Week() {
	var date = new Date();
	if (date.getDay() == 0) { // si on est dimanche, je reviens une semaine en arriere 
		return (new Date(date.setDate(date.getDate() - 7 + 5)));
	}
	return new Date(date.setDate(date.getDate() - date.getDay() + 5));
};

function Thu_Week() {
	var date = new Date();
	if (date.getDay() == 0) { // si on est dimanche, je reviens une semaine en arriere 
		return (new Date(date.setDate(date.getDate() - 7 + 4)));
	}
	return new Date(date.setDate(date.getDate() - date.getDay() + 4));
};

//GENERALISATION des fonctions au dessus : en arguement on donne un indice de 0 a 6 et une date (dimanche a lundi et cela va nous retourner la date correspondant au jour de la semaine)
function WeekDay(date, i) {
	if (date.getDay() == 0) { // si on est dimanche, je reviens une semaine en arriere avant de lire le jour suivant
		return (new Date(date.setDate(date.getDate() - 7 + i)));
	}
	return new Date(date.setDate(date.getDate() - date.getDay() + i));
};
//fonction qui a partir d'une date et de H et M  retourne un timestamp YYYY-MM-DDTHH:MM:00 a cette date
function timestamp(date, ho, mi) {
	return (toY_M_D(date) + 'T' + pad(ho.toString()) + ':' + pad(mi.toString()) + ':00');
};

//fonction qui a partir de deux timestamps, prends la date du premier et l'heure/min du deuxieme pour créer un timestamp
function fuse_timestamp(date1, date2) {
	return (toY_M_D(new Date(date1)) + 'T' + toH_M_S(new Date(date2)));
};

//fonction qui pour une date donné, retourne true si elle est bien entre les plages inf et sup du calendar.

function is_valid_date(date) {
	return (date.getTime() >= plageinf.getTime() && date.getTime() < plagesup.getTime());
};

//fonction qui pour une date donnée, retourne si elle depasse la plage horaire de fin  de semestre;
function outofsemester(date) {
	return date.getTime() >= plagesup.getTime();
};

//fonction qui calcule le nbr de crenaux posable pour le format 1h30 ou 1h (CM/TD/TP)
/*function nb_bloc(heuretotale, format) {
	if (format == "01:00") {
		return (heuretotale.toString());
	}
	else {
		return ((heuretotale / 1.5).toString());
	}
}; OLD*/

//fonction qui retourne 1 ou 1,5 en fonction du format actuel de l'environnement (1h ou 1h30)
function get_x_format() {
	var format = sessionStorage['format'];
	if (format == '01:00') {
		return 1;
	}
	else {
		return 1.5;
	}
};

function get_index_grp(UE, x) {
	UE.ListeGroupe.forEach(el => {
		i = 0;
		if (el == x) {
			return 0
		}
		i++;
	})
};


//function qui retourne le nombre de bloc ayant le codeUE le type et le groupe identique (pour les CM groupe pas pris en compte pour l'instant)
function cptblocidentique(Liste, UE, T, G) { // Liste qu'on veut etudier, for each LiCodeUe , for each [CM,TD,TP], for each ligroupe
	var x = 0;
	Liste.forEach(el => {
		////console.log('hey',el.extendedProps.CodeUE == UE);
		if (el.extendedProps.CodeUE == UE) {
			////console.log(el.extendedProps.CodeUE == UE);
			if (el.extendedProps.Type == 'CM' && T == 'CM') {
				console.log(el, "est un cm");
				x++;
			}
			else if (el.extendedProps.Type == T) {
				//console.log(el.extendedProps.Type)
				if (el.extendedProps.Groupe == G) {
					//console.log(el.extendedProps.Groupe)
					x++;
				}
			}
		}
	});
	return x;
};

//function qui retourne un array qui contient la liste des event ayant meme type groupe code UE(pour les CM groupe pas pris en compte pour l'instant)
function getblocidentique(Liste, UE, T, G) {
	var x = [];
	Liste.forEach(el => {
		if (el.extendedProps.CodeUE == UE) {
			if (el.extendedProps.Type == 'CM' && T == 'CM') {
				x.push(el);
			}
			/*f(el.extendedProps.Type == "A" && el.extendedProps.Type == "CM" ){
				x.push(el);
			}*/
			else if (el.extendedProps.Type == T) {
				//console.log("on est bien sur un tp ?",el.extendedProps.Type == T );
				if (el.extendedProps.Groupe == G) {
					//console.log("on est bien sur le groupe A ?",el.extendedProps.Groupe == G )
					//console.log(el, " ",x);
					x.push(el);
				}
			}
		}
	});
	return x;
};

//fonction qui verifie les deux premiers caractère d'un element a drop pour determiner son groupe
function course(str_course) {
	if (str_course.startsWith('CM')) {
		return 'CM';
	}
	else if (str_course.startsWith('TD')) {

		return 'TD';
	}
	else if (str_course.startsWith('TP')) {
		return 'TP';
	}
	else {
		alert("Erreur de fonction 'wichCourse'");
	}
};

// window.onload = function () { loadExtEvents(), 5000};

/*app.use( function(req, res, next) {

	if (req.originalUrl && req.originalUrl.split("/").pop() === 'favicon.ico') {
	  return res.sendStatus(204);
	}
  
	return next();
  
  });
  */

//function qui associe CM a 0, TD a 1 et TP a 2;
function associate(type) {
	if (type == 'CM') {
		return 0;
	};
	if (type == 'TD') {
		return 1;
	};
	if (type == 'TP') {
		return 2;
	};
};

function gethtype(type) {
	if (type == 'CM') {
		return this.CM;
	};
	if (type == 'TD') {
		return this.TD;
	};
	if (type == 'TP') {
		return this.TP;
	};
};

//function qui ajoute 1 semaine a une date.
function adding1week(date) {
	date.setDate(date.getDate() + 7);
}

//fonction qui compare deux evenement en fonction de leurs date
function compare(event1, event2) {
	if (event1.start < event2.start) {
		return -1;
	}
	if (event1.start > event2.start) {
		return 1;
	}
	return 0;
};




//fonction qui a partir d'un event, va retourner les couleurs qui  couleur associé  
function getcolor(event) {
	var UE = event.extendedProps.UE;
	var res = UE.color
	console.log("getcolor : ", res);
	return res;
}

function getcolorborder(event) {
	if (event.extendedProps.Type == "CM") {
		return "black";
	}
	if (event.extendedProps.Type == "TD") {
		return "blue";
	}
	if (event.extendedProps.Type == "TP") {
		return "green";
	}
};

function filtreUE(codeUE, liste) {
	var res = [];
	liste.forEach(el => {
		if (el._def.extendedProps.CodeUE == codeUE) {
			res.push(el);
		}
	});
	return res;
};


function filtretype(type, liste) {
	var res = [];
	liste.forEach(el => {
		if (el._def.extendedProps.Type == type) {
			res.push(el);
		}
	});
	return res;
};

function getJour(crenaux) {
	x = crenaux._instance.range.start.getDay();
	switch (x) {
		case (1):
			return "Lundi";
			break;
		case (2):
			return "Mardi";
			break;
		case (3):
			return "Mercredi";
			break;
		case (4):
			return "Jeudi";
			break;
		case (5):
			return "Vendredi";
			break;
	}
};

function getdure(crenaux) {
	ah = pad(crenaux._instance.range.start.getUTCHours()).toString();
	am = pad(crenaux._instance.range.start.getUTCMinutes()).toString();
	bh = pad(crenaux._instance.range.end.getUTCHours()).toString();
	bm = pad(crenaux._instance.range.end.getUTCMinutes()).toString();
	return ah + "h" + am + "-" + bh + "h" + bm;
};

function format_DJG(crenaux) {
	return [getJour(crenaux), getdure(crenaux), crenaux._def.extendedProps.Groupe];

}

function convertall_DJG(liste) {
	var x = [];
	liste.forEach(el => {
		var y = 0;
		var z = format_DJG(el);
		x.forEach(ele => {
			if (ele[0] == z[0]) {
				if (ele[1] == z[1]) {
					if (ele[2] == z[2]) {
						y = 1;
					}
				}
			}
		});
		if (y == 0) {
			x.push(format_DJG(el));
		};
	});
	return x;
};

//exports.nom_fonction = nom_fonction;
