console.log("In param.js");

//Variable utile 

let plageinf = new Date(2021, 08, 01);
let plagesup = new Date(2022, 05, 01); //mois -1

function toY_M_D(item) { return item.getFullYear().toString() + "-" + pad(item.getMonth() + 1).toString() + "-" + pad(item.getDate()).toString() };

function Mon_Week() {
	var date = new Date();
	if (date.getDay() == 0) { // si on est dimanche, je reviens une semaine en arriere 
		return (new Date(date.setDate(date.getDate() - 7 + 1)));
	}
	return new Date(date.setDate(date.getDate() - date.getDay() + 1));

};

function Sat_Week() {
	var date = new Date();
	if (date.getDay() == 0) { // si on est dimanche, je reviens une semaine en arriere 
		return (new Date(date.setDate(date.getDate() - 7 + 6)));
	}
	return new Date(date.setDate(date.getDate() - date.getDay() + 6));
};


function option_classic(liCodeUe,LiUE) {
  return (
    {
      initialView: 'timeGridWeek',
      views: {
        timeGridWeek: {
          eventMinHeight: 15,
          allDaySlot: false,
          slotMinTime: "08:00:00",
          slotMaxTime: "20:00:00",
          slotDuration: "00:30:00",
          expandRows: true
        }
      },
      weekends: false,
      eventDurationEditable: false,
      initialDate: "2021-09-01",
      validRange: {
        start: plageinf,
        end: plagesup
      },
      locale: 'fr',
      editable: true,
      droppable: true,
      weekNumbers: true,
      eventOrder: "Type, Groupe",
      drop: function (info) {
        uecheck = LiUE[info.draggedEl.id];
        wichCourse = course(info.draggedEl.childNodes[0].childNodes[0].innerText);
        switch (wichCourse) {
          case 'CM':
            uecheck.ajoutCM();
            reloadEvents(liCodeUe,LiUE)
            break;

          case "TD":
            var grp = info.draggedEl.childNodes[0].childNodes[0].innerText.substr(3);
            uecheck.ajoutTDGroupe(grp);
            reloadEvents(liCodeUe,LiUE)
            break;

          case 'TP':
            var grp = info.draggedEl.childNodes[0].childNodes[0].innerText.substr(3);
            uecheck.ajoutTPGroupe(grp);
            reloadEvents(liCodeUe,LiUE)
            break;

          default:
            alert("Erreur!");
            break;
        }

      },
      eventContent: (arg) => {
        if (!ferie(arg.event)) {
          return arg.event.title + " " + getcodeUE(arg.event) + ' ' + getduree(arg.event);
        }
      }
    })
};

function option_type(liCodeUe,LiUE) {
  return (
    {
      headerToolbar: false,
      dayHeaders: false,
      initialView: 'timeGridWeek',
      views: {
        timeGridWeek: {
          eventMinHeight: 15,
          allDaySlot: false,
          slotMinTime: "08:00:00",
          slotMaxTime: "20:00:00",
          slotDuration: "00:30:00",
          expandRows: true,
          
        }
      },
      weekends: false,
      eventDurationEditable: false,
      initialDate: "2021-09-01",
      validRange: {
        start: toY_M_D(Mon_Week()),
        end: toY_M_D(Sat_Week()),
      },
      locale: 'fr',
      editable: true,
      droppable: true,
      weekNumbers: true,
      eventOrder: "Type, Groupe",
      drop: function (info) {
        uecheck = LiUE[info.draggedEl.id];
        wichCourse = course(info.draggedEl.childNodes[0].childNodes[0].innerText);
        switch (wichCourse) {
          case 'CM':
            uecheck.ajoutCM();
            reloadEvents(liCodeUe,LiUE)
            break;

          case "TD":
            var grp = info.draggedEl.childNodes[0].childNodes[0].innerText.substr(3);
            uecheck.ajoutTDGroupe(grp);
            reloadEvents(liCodeUe,LiUE)
            break;

          case 'TP':
            var grp = info.draggedEl.childNodes[0].childNodes[0].innerText.substr(3);
            uecheck.ajoutTPGroupe(grp);
            reloadEvents(liCodeUe,LiUE)
            break;

          default:
            alert("Erreur!");
            break;
        }

      },
      eventContent: (arg) => {
        if (!ferie(arg.event)) {
          ////console.log(arg.event.extendedProps.CodeUE);
          return arg.event.title + " " + getcodeUE(arg.event) + ' ' + getduree(arg.event);
        }
      }
    })
};