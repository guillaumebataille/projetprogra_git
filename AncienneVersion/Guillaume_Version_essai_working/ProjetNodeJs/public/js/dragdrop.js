console.log("In dragdrop.js")


function dragdrop(LiUE) {
    return {
    itemSelector: '.fc-event',
    eventData: function (eventEl) {

        var x;
        if (document.getElementById('1h00').checked) {
            x = document.getElementById('1h00').value;
            document.getElementById('1h30').disabled = true;

        }
        else {
            x = document.getElementById('1h30').value;
            document.getElementById('1h00').disabled = true;
        }
        console.log("voici l'event El",eventEl);
        var UE = eventEl.attributes.id.value;
        console.log("voici l'ue:",LiUE[UE])
        var Color = LiUE[UE].color;
        var t;
        console.log(LiUE);
        if (eventEl.innerText.substr(0, 2) == "CM") {
            return {
                title: eventEl.innerText,
                duration: x,
                UE: LiUE[UE],
                CodeUE: eventEl.attributes.id.value,
                Type: eventEl.innerText.substr(0, 2),
                Groupe: "all",
                eventColor: '#00FF00',
                backgroundColor: Color, // Couleur pour le fond pour chaque UE 
                borderColor: "black",
                textColor: "black"

            };
        }
        if (eventEl.innerText.substr(0, 2) == "TD") {
            return {
                title: eventEl.innerText,
                duration: x,
                UE: LiUE[UE],
                CodeUE: eventEl.attributes.id.value,
                Type: eventEl.innerText.substr(0, 2),
                Groupe: eventEl.innerText.substr(3),
                backgroundColor: Color, // Couleur pour le fond pour chaque UE 
                borderColor: "blue",
                textColor: "blue"
            };

        }
        if (eventEl.innerText.substr(0, 2) == "TP") {
            return {
                title: eventEl.innerText,
                duration: x,
                UE: LiUE[UE],
                CodeUE: eventEl.attributes.id.value,
                Type: eventEl.innerText.substr(0, 2),
                Groupe: eventEl.innerText.substr(3),
                backgroundColor: Color, // Couleur pour le fond pour chaque UE 
                borderColor: "green",
                textColor: "green"
            };

        }
    } } 
};