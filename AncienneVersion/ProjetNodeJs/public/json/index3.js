console.log("In index3.js");
//import { utils, write, writeFile } from 'xlsx';
let xlsx = require ('xlsx');
//let writeFile = require ('xlsx');
let XLSX = require ('sheetjs-style');
//import * as XLSX from 'sheetjs-style';
//import * as Excel from 'exceljs';
let recupJSON = require('./jsontodiv');
//import { recupJSON } from './jsontodiv.js';
//import { format_DJG } from './function.js';

//alert("Importing index3.js");


const calendar2 = [{
    "UE mutualisée :": "OUI/NON",
    "Responsable :": "A renseigner",
}
]
const calendar3 = [{
    "Intervenants :": ""
}
]
const calendar6 = [{
    "+ initiales enseignants si différent à chaque séance": "",
    "+ initiales enseignants si différent à chaque séance": "",
    "+ initiales enseignants si différent à chaque séance": ""
}
]

function ColonneAD(ws) {
    ws['A1'] = { t: 't', v: "Mention :" };
    ws['A2'] = { t: 't', v: "Parcours :" };
    ws['A3'] = { t: 't', v: "Code UE :" };
    ws['A4'] = { t: 't', v: "Intitulé :" };
    var indice = 1;
    for (var i =1; i<=4; i++) {
        ws['A' + indice].s = { // set the style for target cell
            font: {
                sz: 14,
                bold: true,
                underline: true
            },
            alignment: {
                vertical: "center",
                wrapText: '1', // any truthy value here
            }
        };
        ws['D' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            },
            font: {
                sz: 14,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
                wrapText: '1', // any truthy value here
            }
        };
        ws['E' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            }
        };
        ws['F' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            }
        };
        ws['G' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            }
        };
        ws['H' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            }
        };
        ws['I' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            }
        };
        ws['J' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            }
        };
        ws['K' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            }
        };
        ws['L' + indice].s = { // set the style for target cell
            fill: {
                fgColor: {
                    rgb: "FFFFFF"
                }
            }
        };
        indice++;
    }
}
function ColonneAD2(info, ws) {
    var indice = 7;
    for (var i in info) {
        ws['A' + indice] = { t: 't', v: i }; //Du text dans la cellule
        ws['D' + indice] = { t: 't', v: info[i] };
        ws['A' + indice].s = { // set the style for target cell
            font: {
                sz: 14,
                bold: true,
                underline: true
            },
            alignment: {
                vertical: "center",
                //horizontal: "center",
                wrapText: '1', // any truthy value here
            }
        };
        ws['D' + indice].s = { // set the style for target cell
            font: {
                sz: 14,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
                //horizontal: "center",
                wrapText: '1', // any truthy value here
            }
        };
        indice++;
    }
}
function ColonneAD3(info, ws) {
    var indice = 10;
    for (var i in info) {
        ws['A' + indice] = { t: 't', v: i }; //Du text dans la cellule
        ws['D' + indice] = { t: 't', v: info[i] };
        ws['A' + indice].s = { // set the style for target cell
            font: {
                sz: 14,
                bold: true,
                underline: true
            },
            alignment: {
                vertical: "center",
                //horizontal: "center",
                wrapText: '1', // any truthy value here
            },
        };
        ws['D' + indice].s = { // set the style for target cell
            font: {
                sz: 14,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
                //horizontal: "center",
                wrapText: '1', // any truthy value here
            }
        };
        indice++;
    }
}

function ColonneDE(ws) {
    ws['D10'] = { t: 't', v: " Initiales " };
    ws['E10'] = { t: 't', v: " Nom Prénom " };
    ws['D10'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "thin",
                color: "000000"
            },
            left: {
                style: "medium",
                color: "000000"
            },
            top: {
                style: "medium",
                color: "000000"
            },
            bottom: {
                style: "thin",
                color: "000000"
            }
        }
    };
    ws['E10'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "thin",
                color: "000000"
            },
            top: {
                style: "medium",
                color: "000000"

            },
            bottom: {
                style: "thin",
                color: "000000"
            }
        }
    };

    for (var indice = 11; indice <= 15; indice++) {
        ws['D' + indice].s = {
            font: {
                name: "Verdana",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "thin",
                    color: "000000"
                },
                left: {
                    style: "medium",
                    color: "000000"
                },
                top: {
                    style: "thin",
                    color: "000000"
                },
                bottom: {
                    style: "thin",
                    color: "000000"
                }
            }
        };
        ws['E' + indice].s = {
            font: {
                name: "Verdana",
                sz: 12,
                bold: true
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "medium",
                    color: "000000"
                },
                left: {
                    style: "thin",
                    color: "000000"
                },
                top: {
                    style: "thin",
                    color: "000000"

                },
                bottom: {
                    style: "thin",
                    color: "000000"
                }
            }
        };
    }
    ws['D16'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "thin",
                color: "000000"
            },
            left: {
                style: "medium",
                color: "000000"
            },
            top: {
                style: "thin",
                color: "000000"
            },
            bottom: {
                style: "medium",
                color: "000000"
            }
        }
    };
    ws['E16'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "thin",
                color: "000000"
            },
            top: {
                style: "thin",
                color: "000000"

            },
            bottom: {
                style: "medium",
                color: "000000"
            }
        }
    };
}
function ColonneIV(ws) {
    ws['I10'] = { t: 't', v: "Indiquer les créneaux par un X si une salle à réserver par planning " }; //Du text dans la cellule
    ws['I11'] = { t: 't', v: "Indiquer les créneaux par un A si salle hors FDS, préciser la salle à indiquer en \"notes\" " };
    ws['I12'] = { t: 't', v: "Indiquer les créneaux par un I si salle informatisée" };
    ws['R11'] = { t: 't', v: "+ initiales enseignants si différent à chaque séance" }; //Du text dans la cellule
    ws['I10'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            //bold : true, 
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K10'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L10'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['Q10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['I11'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            //bold : true, 
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['Q11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['I12'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            //bold : true, 
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K12'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L12'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['Q12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['R11'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            //bold : true, 
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['S11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['T11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['U11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['V11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
}
function ColonneQV11(ws) {
    for (var indice = 10; indice <= 13; indice += 2) {
        ws['Q' + indice] = { t: 't', v: "+ initiales enseignants si différent à chaque séance" }; //Du text dans la cellule          
        ws['Q' + indice].s = { // set the style for target cell     
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            },
            font: {
                name: "arial",
                sz: 14,
                //bold : true, 
            },
            alignment: {
                vertical: "center",
            }
        };
        ws['R' + indice].s = { // definir le fond en vert 
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['S' + indice].s = { // definir le fond en vert     
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['T' + indice].s = { // definir le fond en vert     
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['U' + indice].s = { // definir le fond en vert 
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
        ws['V' + indice].s = { // definir le fond en vert 
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            }
        };
    }
}
function TableauMT(ws) {
    ws['Q2'] = { t: 't', v: " CM" }; //Du text dans la cellule
    ws['R2'] = { t: 't', v: " TD" };
    ws['S2'] = { t: 't', v: " TP" };
    ws['T2'] = { t: 't', v: " TERRAIN" };
    ws['R6'] = { t: 't', v: " sur la base des prévisions maquettes (fichiers jaunes)" };

    ws['O3'] = { t: 't', v: " CHARGES : " }; //Du text dans la cellule
    ws['N4'] = { t: 't', v: " Multiple de 1h30 : " };
    ws['N5'] = { t: 't', v: " Multiple de 3h : " };
    ws['O6'] = { t: 't', v: " Effectif previsionnel 21-22 :" };

    ws['R6'].s = {
        font: {
            name: "arial",
            sz: 12,
            italic: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "left"
        },

    };
    ws['Q2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['R2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['S2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['T2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['O3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['N3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }
    };
    ws['M3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N4'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            italic: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }
    };
    ws['M4'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['M5'].s = { // set the style for target cell
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N5'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            italic: true,
            sz: 14,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            bottom: {
                style: "thin"
            }
        }
    };
    ws['O6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 13,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            }
        }
    };
    ws['M6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            }
        }
    };
    ws['O4'].s = { // set the style for target cell
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            }
        }
    };
    ws['O5'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            right: {
                style: "thin"
            }
        }
    };
    ws['Q3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['Q5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['Q6'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };

    ws['R4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['R5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['S3'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['S5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T3'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            right: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }

    ws['P2'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['P4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['P6'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['M2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['N2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['O2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
}

function ColonneEH7(ws) {
    ws['E7'] = { t: 't', v: "          Si OUI indiquer les parcours :" };
    ws['H7'] = { t: 't', v: "A renseigner" };
    ws['E7'].s = { // set the style for target cell    
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['F7'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        }
    };
    ws['H7'].s = { // set the style for target cell    
        font: {
            name: "arial",
            sz: 14,
            bold: true,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['I7'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        }
    };
}

function ColonneCM(ws) {
    ws['C19'] = { t: 't', v: "        CM    " };
    ws['C19'].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
function ColonneTD(ws) {
    ws['C26'] = { t: 't', v: "        TD    " };
    ws['C26'].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
function boucleForYellow(ws) {

    ws['H19'] = { t: 't', v: "S1" };
    ws['I19'] = { t: 't', v: "S2" };
    ws['J19'] = { t: 't', v: "S3" };
    ws['K19'] = { t: 't', v: "S4" };
    ws['L19'] = { t: 't', v: "S5" };
    ws['M19'] = { t: 't', v: "S6" };
    ws['N19'] = { t: 't', v: "S7" };
    ws['O19'] = { t: 't', v: "S8" };
    ws['Q19'] = { t: 't', v: "S10" };
    ws['R19'] = { t: 't', v: "S11" };
    ws['S19'] = { t: 't', v: "S12" };
    ws['T19'] = { t: 't', v: "S13" };
    ws['U19'] = { t: 't', v: "S14" };
    ws['V19'] = { t: 't', v: "S15" };
    ws['W19'] = { t: 't', v: "S16" };
    ws['X19'] = { t: 't', v: "S17" };
    ws['A20'] = { t: 't', v: "Jour" };
    ws['B20'] = { t: 't', v: "Créneau" };
    ws['C20'] = { t: 't', v: "Créneau non classique" };
    ws['D20'] = { t: 't', v: "Enseignant" };
    ws['E20'] = { t: 't', v: "Groupe/Série " };
    ws['F20'] = { t: 't', v: "Effectif" };
    ws['G20'] = { t: 't', v: "Salle" };
    ws['P19'] = { t: 't', v: "VACANCES FEVRIER" };
    ws['J20'] = { t: 't', v: "  1  " };
    ws['K20'] = { t: 't', v: "  2  " };
    ws['L20'] = { t: 't', v: "  3  " };
    ws['M20'] = { t: 't', v: "  4  " };
    ws['N20'] = { t: 't', v: "  5  " };
    ws['O20'] = { t: 't', v: "  6  " };
    ws['Q20'] = { t: 't', v: "  7  " };
    ws['R20'] = { t: 't', v: "  8  " };
    ws['S20'] = { t: 't', v: "  9  " };
    ws['T20'] = { t: 't', v: "  10  " };
    ws['U20'] = { t: 't', v: "  11  " };
    ws['V20'] = { t: 't', v: "  12  " };
    ws['W20'] = { t: 't', v: "  13  " };
    ws['X20'] = { t: 't', v: "  14  " };
    ws['I18'] = { t: 't', v: "Session 1 pairs Licence et Master" };
    ws['J18'] = { t: 't', v: "Lundi 17 CM UNIQUEMENT" };
    ws['T18'] = { t: 't', v: "Session 2 impairs Master  - Pas de CM >80 étudiants" };
    ws['W18'] = { t: 't', v: "lundi 18 férié" };


    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'O'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'Q'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '20'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
    ws['P19'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }

    };
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '21'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '20'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '22'].s = {

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }



    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '22'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }

    for (var indice = 'H'.charCodeAt(0); indice <= 'J'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '18'].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    ws['T18'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


    ws['W18'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


}
function BoucleTD2(ws) {

    ws['H26'] = { t: 't', v: "S1" };
    ws['I26'] = { t: 't', v: "S2" };
    ws['J26'] = { t: 't', v: "S3" };
    ws['K26'] = { t: 't', v: "S4" };
    ws['L26'] = { t: 't', v: "S5" };
    ws['M26'] = { t: 't', v: "S6" };
    ws['N26'] = { t: 't', v: "S7" };
    ws['O26'] = { t: 't', v: "S8" };
    ws['Q26'] = { t: 't', v: "S10" };
    ws['R26'] = { t: 't', v: "S11" };
    ws['S26'] = { t: 't', v: "S12" };
    ws['T26'] = { t: 't', v: "S13" };
    ws['U26'] = { t: 't', v: "S14" };
    ws['V26'] = { t: 't', v: "S15" };
    ws['W26'] = { t: 't', v: "S16" };
    ws['X26'] = { t: 't', v: "S17" };
    ws['A27'] = { t: 't', v: "Jour" };
    ws['B27'] = { t: 't', v: "Créneau" };
    ws['C27'] = { t: 't', v: "Créneau non classique" };
    ws['D27'] = { t: 't', v: "Enseignant" };
    ws['E27'] = { t: 't', v: "Groupe/Série " };
    ws['F27'] = { t: 't', v: "Effectif" };
    ws['G27'] = { t: 't', v: "Salle" };
    ws['P26'] = { t: 't', v: "VACANCES FEVRIER" };
    ws['J27'] = { t: 't', v: "  1  " };
    ws['K27'] = { t: 't', v: "  2  " };
    ws['L27'] = { t: 't', v: "  3  " };
    ws['M27'] = { t: 't', v: "  4  " };
    ws['N27'] = { t: 't', v: "  5  " };
    ws['O27'] = { t: 't', v: "  6  " };
    ws['Q27'] = { t: 't', v: "  7  " };
    ws['R27'] = { t: 't', v: "  8  " };
    ws['S27'] = { t: 't', v: "  9  " };
    ws['T27'] = { t: 't', v: "  10  " };
    ws['U27'] = { t: 't', v: "  11  " };
    ws['V27'] = { t: 't', v: "  12  " };
    ws['W27'] = { t: 't', v: "  13  " };
    ws['X27'] = { t: 't', v: "  14  " };
    ws['I25'] = { t: 't', v: "Session 1 pairs Licence et Master" };
    ws['J25'] = { t: 't', v: "Lundi 17 CM UNIQUEMENT" };
    ws['T25'] = { t: 't', v: "Session 2 impairs Master  - Pas de CM >80 étudiants" };
    ws['W25'] = { t: 't', v: "lundi 18 férié" };
    ws['Z18'] = { t: 't', v: "Session 1     semestres pairs    Du 10 au 20 Mai" };
    ws['Z25'] = { t: 't', v: "Session 1     semestres pairs    Du 10 au 20 Mai" };
    ws['Z39'] = { t: 't', v: "Session 1     semestres pairs    Du 10 au 20 Mai" };
    ws['Z32'] = { t: 't', v: "Session 1     semestres pairs    Du 10 au 20 Mai" };


    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '26'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '26'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'O'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '26'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'Q'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '26'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '27'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
    ws['P26'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }

    };
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '28'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '27'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '29'].s = {

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }



    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '29'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 18; indice <= 44; indice++) {
        ws['Z' + indice].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    for (var indice = 27; indice <= 29; indice++) {
        ws['P' + indice].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
            }
        };
    };

    for (var indice = 20; indice <= 22; indice++) {
        ws['P' + indice].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
            }
        };
    };
    for (var indice = 34; indice <= 36; indice++) {
        ws['P' + indice].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
            }
        };
    };
    for (var indice = 'H'.charCodeAt(0); indice <= 'J'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '25'].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    ws['T25'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


    ws['W25'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


}
function ColonneTP(ws) {
    ws['C33'] = { t: 't', v: "        TP    " };
    ws['C33'].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
function ColonneTERRAIN(ws) {
    ws['C40'] = { t: 't', v: "        TERRAIN    " };
    ws['C40'].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
function boucleTP2(ws) {

    ws['H33'] = { t: 't', v: "S1" };
    ws['I33'] = { t: 't', v: "S2" };
    ws['J33'] = { t: 't', v: "S3" };
    ws['K33'] = { t: 't', v: "S4" };
    ws['L33'] = { t: 't', v: "S5" };
    ws['M33'] = { t: 't', v: "S6" };
    ws['N33'] = { t: 't', v: "S7" };
    ws['O33'] = { t: 't', v: "S8" };
    ws['Q33'] = { t: 't', v: "S10" };
    ws['R33'] = { t: 't', v: "S11" };
    ws['S33'] = { t: 't', v: "S12" };
    ws['T33'] = { t: 't', v: "S13" };
    ws['U33'] = { t: 't', v: "S14" };
    ws['V33'] = { t: 't', v: "S15" };
    ws['W33'] = { t: 't', v: "S16" };
    ws['X33'] = { t: 't', v: "S17" };
    ws['A34'] = { t: 't', v: "Jour" };
    ws['B34'] = { t: 't', v: "Créneau" };
    ws['C34'] = { t: 't', v: "Créneau non classique" };
    ws['D34'] = { t: 't', v: "Enseignant" };
    ws['E34'] = { t: 't', v: "Groupe/Série " };
    ws['F34'] = { t: 't', v: "Effectif" };
    ws['G34'] = { t: 't', v: "Salle" };
    ws['P33'] = { t: 't', v: "VACANCES FEVRIER" };
    ws['J34'] = { t: 't', v: "  1  " };
    ws['K34'] = { t: 't', v: "  2  " };
    ws['L34'] = { t: 't', v: "  3  " };
    ws['M34'] = { t: 't', v: "  4  " };
    ws['N34'] = { t: 't', v: "  5  " };
    ws['O34'] = { t: 't', v: "  6  " };
    ws['Q34'] = { t: 't', v: "  7  " };
    ws['R34'] = { t: 't', v: "  8  " };
    ws['S34'] = { t: 't', v: "  9  " };
    ws['T34'] = { t: 't', v: "  10  " };
    ws['U34'] = { t: 't', v: "  11  " };
    ws['V34'] = { t: 't', v: "  12  " };
    ws['W34'] = { t: 't', v: "  13  " };
    ws['X34'] = { t: 't', v: "  14  " };
    ws['I32'] = { t: 't', v: "Session 1 pairs Licence et Master" };
    ws['J32'] = { t: 't', v: "Lundi 17 CM UNIQUEMENT" };
    ws['T32'] = { t: 't', v: "Session 2 impairs Master  - Pas de CM >80 étudiants" };
    ws['W32'] = { t: 't', v: "lundi 18 férié" };


    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '33'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '33'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'O'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '33'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'Q'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '33'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '34'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
    ws['P33'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }

    };
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '35'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '34'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '35'].s = {

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }



    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '36'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }

    for (var indice = 'H'.charCodeAt(0); indice <= 'J'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '32'].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    ws['T32'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


    ws['W32'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


}
function boucleTERRAIN2(ws) {

    ws['H40'] = { t: 't', v: "S1" };
    ws['I40'] = { t: 't', v: "S2" };
    ws['J40'] = { t: 't', v: "S3" };
    ws['K40'] = { t: 't', v: "S4" };
    ws['L40'] = { t: 't', v: "S5" };
    ws['M40'] = { t: 't', v: "S6" };
    ws['N40'] = { t: 't', v: "S7" };
    ws['O40'] = { t: 't', v: "S8" };
    ws['Q40'] = { t: 't', v: "S10" };
    ws['R40'] = { t: 't', v: "S11" };
    ws['S40'] = { t: 't', v: "S12" };
    ws['T40'] = { t: 't', v: "S13" };
    ws['U40'] = { t: 't', v: "S14" };
    ws['V40'] = { t: 't', v: "S15" };
    ws['W40'] = { t: 't', v: "S16" };
    ws['X40'] = { t: 't', v: "S17" };
    ws['A41'] = { t: 't', v: "Jour" };
    ws['B41'] = { t: 't', v: "Créneau" };
    ws['C41'] = { t: 't', v: "Créneau non classique" };
    ws['D41'] = { t: 't', v: "Enseignant" };
    ws['E41'] = { t: 't', v: "Groupe/Série " };
    ws['F41'] = { t: 't', v: "Effectif" };
    ws['G41'] = { t: 't', v: "Salle" };
    ws['P40'] = { t: 't', v: "VACANCES FEVRIER" };
    ws['J41'] = { t: 't', v: "  1  " };
    ws['K41'] = { t: 't', v: "  2  " };
    ws['L41'] = { t: 't', v: "  3  " };
    ws['M41'] = { t: 't', v: "  4  " };
    ws['N41'] = { t: 't', v: "  5  " };
    ws['O41'] = { t: 't', v: "  6  " };
    ws['Q41'] = { t: 't', v: "  7  " };
    ws['R41'] = { t: 't', v: "  8  " };
    ws['S41'] = { t: 't', v: "  9  " };
    ws['T41'] = { t: 't', v: "  10  " };
    ws['U41'] = { t: 't', v: "  11  " };
    ws['V41'] = { t: 't', v: "  12  " };
    ws['W41'] = { t: 't', v: "  13  " };
    ws['X41'] = { t: 't', v: "  14  " };
    ws['I39'] = { t: 't', v: "Session 1 pairs Licence et Master" };
    ws['J39'] = { t: 't', v: "Lundi 17 CM UNIQUEMENT" };
    ws['T39'] = { t: 't', v: "Session 2 impairs Master  - Pas de CM >80 étudiants" };
    ws['W39'] = { t: 't', v: "lundi 18 férié" };


    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '40'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '40'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'O'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '40'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'Q'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '40'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '41'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
    ws['P40'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }

    };
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '42'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'H'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '41'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '42'].s = {

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }
    for (var indice = 'A'.charCodeAt(0); indice <= 'X'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '43'].s = {
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };

    }

    for (var indice = 'H'.charCodeAt(0); indice <= 'J'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '39'].s = {
            fill: {
                fgColor: { rgb: "C0DFEF" }
            },
            font: {
                name: "arial",
                sz: 8,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: true
            },

        };

    }
    ws['T39'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


    ws['W39'].s = {
        fill: {
            fgColor: { rgb: "C0DFEF" }
        },
        font: {
            name: "arial",
            sz: 8,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: true
        },

    };


}
function QRS(ws){

    for (var indice = 'Q'.charCodeAt(0); indice < 'T'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
    ws[lettre + '3'].s = {
        font: {
            name : "arial",
            sz: 14,
            bold : true,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            },
        border: {
            bottom: {
                style: "thin"
                },
            right: {
                style: "thin"
                },
            left: {
                style: "thin"
                }
            }
         };
         ws[lettre + '4'].s = {
            font: {
                name : "arial",
                sz: 14,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                },
            border: {
                bottom: {
                    style: "thin"
                    },
                right: {
                    style: "thin"
                    },
                left: {
                    style: "thin"
                    }
                }
             };
    }
    ws['S5'].s = {
        font: {
            name : "arial",
            sz: 14,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            },
        border: {
            bottom: {
                style: "thin"
                },
            right: {
                style: "thin"
                },
            left: {
                style: "thin"
                }
            }
         };
}
function convertJsonToExcel() {
    const workBook = xlsx.utils.book_new();
    var contenuJSON = recupJSON();
    var y = [];

    var CM = new Object;
    var TD = new Object;
    var TP = new Object;
    var NomUE = new Object;

    contenuJSON.forEach(Objet => {
        y.push(Objet.CodeUE);
        CM[Objet.CodeUE]= Objet.CM;
        TD[Objet.CodeUE]= Objet.TD;
        TP[Objet.CodeUE]= Objet.TP;
        NomUE[Objet.CodeUE]= Objet.NomUE;

        console.log("CM :", Objet['CM']);
        console.log("Nom UE :", Objet['NomUE']);

    });

    // y contient la liste des codeUE - array
    // CM contient la liste des h de CM - dico
    // TD contient la liste des h de TD - dico
    // TP contient la liste des h de TP - dico 

   // console.log("voici les UEs differents :", y);

  /*  y.forEach(item => {     
        console.log("UE : ", item, " CM : ", CM[item], " TD : ", TD[item], " TP : ", TP[item], "Nom UE", NomUE[item]);
    });*/
  


    for (var key in y) {
        // Je crée une feuille vide par UE
        var uneligne = [];
        for (let i = 1; i < 50; i++)
            uneligne.push("");
        var table = [];
        for (let i = 1; i < 50; i++)
            table.push(uneligne);

        var ws = xlsx.utils.aoa_to_sheet(table);
        xlsx.utils.book_append_sheet(workBook, ws, y[key]);

        ws['D3'] = { t: 't', v: y[key] };
        ws['D4'] = {t : 't', v: NomUE[y[key]]};
        ws['Q3'] = { t: 't', v: CM[y[key]] };
        ws['R3'] = { t: 't', v: TD[y[key]] };
        ws['S3'] = { t: 't', v: TP[y[key]] };
        ws['Q4'] = { t: 't', v: CM[y[key]]/1.5 };
        ws['R4'] = { t: 't', v: TD[y[key]]/1.5 };
        ws['S4'] = { t: 't', v: TP[y[key]]/1.5 };
        if (TP[y[key]]%3==0) {ws['S5']={t: 't', v:TP[y[key]]/3}}
        else {ws['S5']={t: 't', v:'0'}}

        for (const info2 of calendar2) {
            ColonneAD2(info2, ws);
        }
        for (const info3 of calendar3) {
            ColonneAD3(info3, ws);
        }

        ColonneAD(ws);
        ColonneDE(ws);
        ColonneIV(ws);
        ColonneQV11(ws);
        TableauMT(ws);
        ColonneEH7(ws);
        ColonneCM(ws);
        boucleForYellow(ws);
        BoucleTD2(ws);
        ColonneTD(ws);
        ColonneTP(ws);
        ColonneTERRAIN(ws);
        boucleTP2(ws);
        boucleTERRAIN2(ws);
        QRS(ws);


        ws['!cols'] = [{ width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 25 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }, { width: 7 }]; //set col. widths
        ws['!rows'] = [{ hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, // set row. heights
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 80 }, { hpx: 80 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 60 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 60 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 60 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 }];

        xlsx.writeFile(workBook, "Calendar2.xlsx");
        XLSX.writeFile(workBook, 'Calendar2.xlsx');
    }
};

