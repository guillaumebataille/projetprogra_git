let express = require("express");

let app = express();

let session = require('express-session');

//let recupJSON = require('./public/js/jsontodiv');

//let exp = require('./json/index3');




//param = require ("./public/js/param");

//let param = require('./param.js');

//console.log(param.view());

//Moteur de template
app.set('view engine', 'ejs');

//Middleware
app.use('/assets', express.static('public'));
app.use(express.static('public'));
app.use("/js",express.static(__dirname + 'public/js'));
app.use("/css",express.static(__dirname + 'public/css'));
app.use("/json",express.static(__dirname + 'public/json'));
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(session({
    secret: 'yoyo',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));
app.use(require('./middlewares/flash')); 



//Routes
app.get("/", (request, response) => {
    //response.render('pages/test3', {LiUE : recupJSON()});
    response.render('pages/test2', {link : "test3.ejs", page : "Semainier - TYPE"});
});
app.get("/test3.ejs",(request, response) => {
    response.render('pages/test3', {link : "test2.ejs", page : "Semainier - MALEABLE"});
});

app.get("/test2.ejs", (request, response) => {
    //response.render('pages/test3', {LiUE : recupJSON()});
    response.render('pages/test2', {link : "test3.ejs", page : "Semainier - TYPE"});
});

app.get("/test4.ejs", (request, response) => {
    //response.render('pages/test3', {LiUE : recupJSON()});
    let exp = require('./json/index3');
    response.render('pages/test3', {link : "test2.ejs", page : "Semainier - MALEABLE"});
    console.log(request.body.essai)
});

app.post("/", (request, response) => {
    if (request.body.message === undefined || request.body.message === '') {
        request.flash('error', "Vous n'avez pas posté de message.")
        response.redirect("/");
    } else {

    }
});

//let excel = require('./json/index3')
//let f= require('./function/function');

//console.log(f.JoursFeries(2021)); 

//let cal = require('@fullcalendar/core');



app.listen(8080);

