var XLSX = require("xlsx");
var fs = require("fs");

console.log("In import.js");
//Reading the Excel File
const wb = XLSX.readFile("./Trame.ods");
//const wb = XLSX.readFile("./Trame.ods");

// Read a specific sheet from the workbook
const ws = wb.Sheets["UE FdS"];
//console.log(ws);
//console.log(wb.SheetNames);

//Read the sheet data and conversion to JSON
const data = XLSX.utils.sheet_to_json(ws);
//console.log(data);

//Removing the doublons
let t = [];
let y;
data.forEach((en) => {
  y = 0;
  t.forEach((el) => {
    if (en["Code UE"] == el["Code UE"]) {
      y = 1;
    }
  });
  if (y == 0) {
    t.push(en);
  }
});

function filtreUE(UEfi, uniqueUE) {
  console.log("from Import uniqueUE: ", uniqueUE);
  var p = t.filter((x) => x["Code UE"].slice(0, 4) == UEfi);
  uniqueUE.forEach((uniqueue) => {
    var item = new Map();
    item = t.filter((x) => x["Code UE"] == uniqueue);
    //console.log("item", item[0]);
    if (item[0] != undefined) {
      console.log(item);
      p.push(item[0]);
    }
  });
  console.log(p);
  fs.writeFileSync("./public/json/importUE.json", JSON.stringify(p, null, 2));
  return p;
}

exports.filtreUE = filtreUE;
