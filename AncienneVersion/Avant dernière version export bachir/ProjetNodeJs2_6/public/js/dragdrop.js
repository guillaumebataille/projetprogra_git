console.log("In dragdrop.js");
sessionStorage.id = 0;

function dragdrop(LiUE) {
  return {
    itemSelector: ".fc-event",
    eventData: function (eventEl) {
      var x = sessionStorage["format"];

      console.log("voici l'event El", eventEl);
      var UE = eventEl.attributes.id.value;
      console.log("voici l'ue:", LiUE[UE]);
      console.log("voici son code UE", LiUE[UE].CodeUE);
      var Color = LiUE[UE].color;
      var t;
      console.log(LiUE);
      if (eventEl.innerText.substr(0, 2) == "CM") {
        return {
          title: eventEl.innerText,
          duration: x,
          UE: LiUE[UE],
          Informatise: 0,
          Intervenant: "",
          Libellé: LiUE[UE].LiUE,
          CodeUE: LiUE[UE].CodeUE,
          Type: eventEl.innerText.substr(0, 2),
          Groupe: "all",
          // eventColor: "#00FF00",
          backgroundColor: Color[0], // Couleur pour le fond pour chaque UE
          borderColor: "#C0C0C0",
          textColor: "#C0C0C0",
        };
      }
      if (eventEl.innerText.substr(0, 2) == "TD") {
        return {
          title: eventEl.innerText,
          duration: x,
          UE: LiUE[UE],
          Informatise: 0,
          Intervenant: "",
          Libellé: LiUE[UE].Libellé,
          CodeUE: LiUE[UE].CodeUE,
          Type: eventEl.innerText.substr(0, 2),
          Groupe: eventEl.innerText.split("-")[1].split(" ")[0],
          backgroundColor: Color[1], // Couleur pour le fond pour chaque UE
          borderColor: "#808080",
          textColor: "#808080",
        };
      }
      if (eventEl.innerText.substr(0, 2) == "TP") {
        return {
          title: eventEl.innerText,
          duration: x,
          UE: LiUE[UE],
          Informatise: 0,
          Intervenant: "",
          Libellé: LiUE[UE].Libellé,
          CodeUE: LiUE[UE].CodeUE,
          Type: eventEl.innerText.substr(0, 2),
          Groupe: eventEl.innerText.split("-")[1].split(" ")[0],
          backgroundColor: Color[2], // Couleur pour le fond pour chaque UE
          borderColor: "#000000",
          textColor: "#000000",
        };
      }
    },
  };
}
