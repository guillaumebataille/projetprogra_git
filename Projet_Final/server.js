let express = require("express");

let app = express();

let session = require("express-session");

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

//let recupJSON = require('./public/js/jsontodiv');

//let exp = require('./json/index3');

var ex = require("./json/index3");
var imp = require("./json/Import");
var save = require("./json/saving");
var load = require("./json/loading");
//var ue = require('./public/js/UE');
//var fs = require('fs');
const { waitForDebugger } = require("inspector");
const { format } = require("path");
const { stripVTControlCharacters } = require("util");
//var x = require ('./public/json/index3.mjs');
//console.log(ex.hello);
let LiCren;
let LiFer;
var binf; // new // A update
var bsup; //new // A update
var vac = []; //new // A update
var UEfi; //new // A update
var Liste_Groupe = new Object(); //new // Old
var array_groupe = []; //new // A update
var SS = "0"; //new // A update
var LiMal = "0"; //new // A update
var uniqueUE;
//console.log(ue.recupJSON());

//param = require ("./public/js/param");

//let param = require('./param.js');

//console.log(param.view());

//Moteur de template
app.set("view engine", "ejs");

//Middleware
app.use("/assets", express.static("public"));
app.use(express.static("public"));

app.use("/js", express.static(__dirname + "public/js"));
app.use("/css", express.static(__dirname + "public/css"));
app.use("/json", express.static(__dirname + "public/json"));

app.use(express.urlencoded({ extended: false, limit: "50mb" }));
app.use(express.json({ limit: "50mb" }));

app.use(
  session({
    secret: "yoyo",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false },
  })
);

app.use(require("./middlewares/flash"));

//Routes
let err;
app.get("/", (request, response) => {
  if (request.query.error == undefined) {
    err = 1;
  } else {
    err = request.query.error;
  }
  console.log(err);
  //response.render('pages/test3', {LiUE : recupJSON()});
  response.render("pages/form", {
    link: "test3.ejs",
    page: "Semainier - TYPE",
    err: err,
  }); //TEST FOR UEDYNA
  //response.render('pages/test2',{ link: "test3.ejs", page: "Semainier - TYPE" });
});

app.get("/test3.ejs", (request, response) => {
  response.render("pages/test3", {
    link: "test2.ejs",
    page: "Semainier - MALEABLE",
    binf: binf,
    bsup: bsup,
    vac: [vac],
    UEfi: UEfi,
    Liste_Groupe: [array_groupe],
    SS: SS,
    LiMal: LiMal,
  });
});

app.get("/test2.ejs", (request, response) => {
  //response.render('pages/test3', {LiUE : recupJSON()});
  response.render("pages/test2", {
    link: "test3.ejs",
    page: "Semainier - TYPE",
    Liste_Groupe: [array_groupe],
    SS: SS,
    LiMal: LiMal,
  });
});

app.post("/verif.ejs", (request, response) => {
  //response.render('pages/test3', {LiUE : recupJSON()});
  //console.log("le coté serveur après verif : ", request.body);
  binf = request.body.binf;
  bsup = request.body.bsup;
  vac = request.body.vac;
  grp = request.body.grp;
  grptype = request.body.grpglobal;

  // console.log("grptype", grptype);
  array_groupe = [];
  if (grptype == 1) {
    //console.log("ici?");
    imp.filtreUE(UEfi, uniqueUE).forEach((UE) => {
      //console.log(grp);
      if (typeof grp == typeof "a") grp = [grp];
      // Liste_Groupe[UE["Code UE"]] = grp;
      array_groupe.push(grp);
      array_groupe.push("$");
    });
  } else {
    //console.log("Bine");
    array_groupe = [];
    imp.filtreUE(UEfi, uniqueUE).forEach((UE) => {
      var i = imp.filtreUE(UEfi, uniqueUE).indexOf(UE).toString();
      var x =
        typeof request.body["grp" + i.toString()] == typeof "a"
          ? [request.body["grp" + i.toString()]]
          : request.body["grp" + i.toString()];
      //Liste_Groupe[UE["Code UE"]] = x;
      array_groupe.push(x);
      array_groupe.push("$");
    });
  }
  //console.log("Liste groupe :", Liste_Groupe);
  //console.log("array", array_groupe);

  response.render("pages/test2", {
    link: "test3.ejs",
    page: "Semainier - TYPE",
    Liste_Groupe: [array_groupe],
    SS: SS,
    LiMal: LiMal,
  });
});

app.get("/test4.ejs", (request, response) => {
  //response.render('pages/test3', {LiUE : recupJSON()});
  response.render("pages/test3", {
    link: "test2.ejs",
    page: "Semainier - MALEABLE",
  });
  // console.log(request.body.essai);
});

app.post("/", (req, res) => {
  console.log("Working");
  //console.log("Je suis dans /", req.body);
  binf = req.body.borneinf; //recup borne inf
  bsup = req.body.bornesup; //recup borne sup
  vac = req.body.vac;
  UEfi = req.body.UEfilter;
  heure = req.body.format;
  if (typeof req.body.ue == "string" && req.body.ue != undefined) {
    uniqueUE = [req.body.ue];
  } else {
    uniqueUE = req.body.ue;
  }
  console.log("UniqueUE: ", uniqueUE);

  //console.log(binf);
  var UEfiltred = imp.filtreUE(UEfi, uniqueUE);
  var UEfil = [];
  var LisUE = [];
  UEfiltred.forEach((element) => {
    UEfil.push(element["Libellé"]);
    LisUE.push(element["Code UE"]);
  });
  if (UEfiltred.length == 0) {
    res.redirect("/?error=" + UEfiltred.length.toString());
  } else {
    res.render("pages/verif", {
      binf: binf,
      bsup: bsup,
      vac: vac,
      UEfiltred: UEfil,
      LisUE: LisUE,
      format: heure,
      SS: SS,
      LiMal: LiMal,
    });
  }
  //fs.writeFileSync("./public/json/importUE.json",JSON.stringify(UEfiltred), null,2);
  //res.redirect('/test2.ejs');
  //res.redirect('/test.', { test : "Vous avec actueellement"});

  //res.render('pages/test2');
});

app.post("/listecrenaux", (req, res) => {
  // you have address available in req.body:

  LiCren = JSON.parse(req.body.listecrenaux);
  LiFer = JSON.parse(req.body.listeferie);
  ex.generateExcel(LiCren, LiFer, binf, bsup, vac);
  // appel function export dynamique
  console.log("jours féries", LiFer);
  console.log("Successful export");
  // always send a response:
  //console.log('Voici la liste des crenaux', LiCren);
  //console.log('Voici la liste des jours ferié', LiFer);
  res.json({ ok: true });
});

app.post("/loading", (req, response) => {
  // console.log("bam");
  //console.log(req.body);
  var resp = load.loading();
  binf = resp[0];
  bsup = resp[1];
  vac = resp[2];
  array_groupe = resp[4];
  SS = resp[5];
  LiMal = resp[6];
  uniqueUE = resp[7];
  UEfiltred = imp.filtreUE(resp[3], uniqueUE); // Y ajouter la liste d'ue unique si besoin
  /* console.log("binf", binf);
  console.log("bsup", bsup);
  console.log("vac", vac);
  console.log("Uefiltré ", UEfiltred);
  console.log(array_groupe);
  console.log("arrivée a page render ");*/
  //console.log("SS ", SS);
  response.render("pages/test3", {
    link: "test2.ejs",
    page: "Semainier - MALEABLE",
    binf: binf,
    bsup: bsup,
    vac: [vac],
    UEfi: UEfi,
    Liste_Groupe: [array_groupe],
    SS: SS,
    LiMal: LiMal,
  });
  SS = "0";
  LiMal = "0";
  //  console.log("end");
  // 0 = binf // 1 = bsup // 2 = vac // 3 = UEfi // 4 = listegroupe // 5 = SessionS // 6 = ListeCrenauxMalleable
});

app.post("/saving", (req, res) => {
  //console.log("saving contenu ", req.body);
  var LiUEfiltre = req.body.LiUEfiltre;
  // console.log(LiUEfiltre);
  // console.log("UEFI : ", UEfi);
  // console.log("Listedesgroupes : ", array_groupe);
  var SessionSto = req.body.sessionS;
  console.log("SessionSto ", SessionSto);
  var listemal = req.body.listecrenaux;
  var array_saved = save.saving(
    binf,
    bsup,
    vac,
    LiUEfiltre,
    array_groupe,
    SessionSto,
    listemal,
    uniqueUE
  );
  array_saved.forEach((item) => {
    //  console.log("item 1 = ", item);
    // 0 = binf // 1 = bsup // 2 = vac // 3 = UEfi // 4 = listegroupe // 5 = SessionS // 6 = ListeCrenauxMalleable
  });
});

//let excel = require('./json/index3')
//let f= require('./function/function');

//console.log(f.JoursFeries(2021));

//let cal = require('@fullcalendar/core');

app.listen(8080);
