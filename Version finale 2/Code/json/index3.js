//les librairies que j'ai utilisé
let xlsx = require ('xlsx');
let XLSX = require ('sheetjs-style');
//la fonction qu'on utilise pour récupérer le fichier json
let recupJSON = require('./jsontodiv');
// la fonction qui nous génère TOUT le fichier excel final
function generateExcel(LiCren, LiFer,binf, bsup, vac){

//la partie statique de la trame    
function Statique(ws) {
    ws['A1'] = { t: 't', v: "Mention :" };
    ws['A2'] = { t: 't', v: "Parcours :" };
    ws['A3'] = { t: 't', v: "Code UE :" };
    ws['A4'] = { t: 't', v: "Intitulé :" };
    ws['A4'] = { t: 't', v: "Intitulé :" };
    ws['D1'] = { t: 't', v: "A renseigner" };
    ws['D2'] = { t: 't', v: "A renseigner" };
    ws['A7'] = { t: 't', v: "UE mutualisée :" };
    ws['D7'] = { t: 't', v: "OUI/NON" };
    ws['A8'] = { t: 't', v: "Responsable :" };
    ws['D8'] = { t: 't', v: "A renseigner" };    
    ws['A10'] = { t: 't', v: "Intervenants :" };
    ws['D10'] = { t: 't', v: " Initiales " };
    ws['E10'] = { t: 't', v: " Nom Prénom " };
    ws['I10'] = { t: 't', v: "Indiquer les créneaux par un X si une salle à réserver par planning " }; //Du text dans la cellule
    ws['I11'] = { t: 't', v: "Indiquer les créneaux par un A si salle hors FDS, préciser la salle à indiquer en \"notes\" " };
    ws['I12'] = { t: 't', v: "Indiquer les créneaux par un I si salle informatisée" };
    ws['N11'] = { t: 't', v: "+ initiales enseignants si différent à chaque séance" };
    ws['Q2'] = { t: 't', v: " CM" }; //Du text dans la cellule
    ws['R2'] = { t: 't', v: " TD" };
    ws['S2'] = { t: 't', v: " TP" };
    ws['T2'] = { t: 't', v: " TERRAIN" };
    ws['R6'] = { t: 't', v: " sur la base des prévisions maquettes (fichiers jaunes)" };

    ws['O3'] = { t: 't', v: " CHARGES : " }; //Du text dans la cellule
    ws['N4'] = { t: 't', v: " Multiple de 1h30 : " };
    ws['N5'] = { t: 't', v: " Multiple de 3h : " };
    ws['O6'] = { t: 't', v: " Effectif previsionnel 21-22 :" };
    ws['E7'] = { t: 't', v: "          Si OUI indiquer les parcours :" };
    ws['G7'] = { t: 't', v: "A renseigner" };
    ws['C19'] = { t: 't', v: "        CM    " };
    ws['A20'] = { t: 't', v: "Jour" };
    ws['B20'] = { t: 't', v: "Créneau" };
    ws['C20'] = { t: 't', v: "Créneau non classique" };
    ws['D20'] = { t: 't', v: "Enseignant" };
    ws['E20'] = { t: 't', v: "Groupe/Série " };
    ws['F20'] = { t: 't', v: "Effectif" };
    ws['G20'] = { t: 't', v: "Salle" };
 

    var indice = 1;
    for (var i =1; i<=4; i++) {
        ws['A' + indice].s = {
            font: {
                name : "Arial",
                sz: 14,
                bold: true,
                underline: true
            },
            alignment: {
                vertical: "center",
                wrapText: '1', 
            }
        };
        ws['D' + indice].s = { 
            font: {
                name : "Arial",
                sz: 14,
                bold: true,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
            }
        };
        indice++;
    }
    ws['A7'].s = { // set the style for target cell
        font: {
            name : "Arial",
            sz: 14,
            bold: true,
            underline: true
        },
        alignment: {
            vertical: "center",
            wrapText: '1', 
        }
    };
ws['A8'].s = { // set the style for target cell
        font: {
            name : "Arial",
            sz: 14,
            bold: true,
            underline: true
        },
        alignment: {
            vertical: "center",
            wrapText: '1', 
        }
    };
    ws['D7'].s = { 
        font: {
            name : "Arial",
            sz: 14,
            bold: true,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
            wrapText: '1', 
        }
    };
    ws['D8'].s = { 
        font: {
            name : "Arial",
            sz: 14,
            bold: true,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
            wrapText: '1', 
        }
    };
    ws['A10' ].s = { 
        font: {
            name : "Arial",
            sz: 14,
            bold: true,
            underline: true
        },
        alignment: {
            vertical: "center",
            wrapText: '1', 
        },
    };
    ws['D10'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "thin",
                color: "000000"
            },
            left: {
                style: "medium",
                color: "000000"
            },
            top: {
                style: "medium",
                color: "000000"
            },
            bottom: {
                style: "thin",
                color: "000000"
            }
        }
    };
    ws['E10'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "thin",
                color: "000000"
            },
            top: {
                style: "medium",
                color: "000000"

            },
            bottom: {
                style: "thin",
                color: "000000"
            }
        }
    };

    for (var indice = 11; indice <= 15; indice++) {
        ws['D' + indice].s = {
            font: {
                name: "Verdana",
                sz: 12,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "thin",
                    color: "000000"
                },
                left: {
                    style: "medium",
                    color: "000000"
                },
                top: {
                    style: "thin",
                    color: "000000"
                },
                bottom: {
                    style: "thin",
                    color: "000000"
                }
            }
        };
        ws['E' + indice].s = {
            font: {
                name: "Verdana",
                sz: 12,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "medium",
                    color: "000000"
                },
                left: {
                    style: "thin",
                    color: "000000"
                },
                top: {
                    style: "thin",
                    color: "000000"

                },
                bottom: {
                    style: "thin",
                    color: "000000"
                }
            }
        };
    }
    ws['D16'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "thin",
                color: "000000"
            },
            left: {
                style: "medium",
                color: "000000"
            },
            top: {
                style: "thin",
                color: "000000"
            },
            bottom: {
                style: "medium",
                color: "000000"
            }
        }
    };
    ws['E16'].s = {
        font: {
            name: "Verdana",
            sz: 12,
            bold: true
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "thin",
                color: "000000"
            },
            top: {
                style: "thin",
                color: "000000"

            },
            bottom: {
                style: "medium",
                color: "000000"
            }
        }
    };
    ws['I10'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "Arial",
            sz: 14,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K10'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L10'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P10'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['I11'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "Arial",
            sz: 14,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L11'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P11'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['I12'].s = { // set the style for target cell     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['J12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['K12'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['L12'].s = { // definir le fond en vert     
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['M12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['N12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['O12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };
    ws['P12'].s = { // definir le fond en vert 
        fill: {
            fgColor: {
                rgb: "8A9A5B"
            }
        }
    };

    for (var indice = 10; indice <= 13; indice += 2) {
        ws['M' + indice] = { t: 't', v: "+ initiales enseignants si différent à chaque séance" }; //Du text dans la cellule          
        ws['M' + indice].s = { // set the style for target cell     
            fill: {
                fgColor: {
                    rgb: "8A9A5B"
                }
            },
            font: {
                name: "Arial",
                sz: 14,
            },
            alignment: {
                vertical: "center",
            }
        };
    }

    ws['R6'].s = {
        font: {
            name: "arial",
            sz: 12,
            italic: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "left"
        },

    };
    ws['Q2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['R2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['S2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['T2'].s = {
        font: {
            name: "arial",
            sz: 10,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['O3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['N3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }
    };
    ws['M3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N4'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            italic: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        }
    };
    ws['M4'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['M5'].s = { // set the style for target cell
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N5'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            italic: true,
            sz: 14,
        },
        alignment: {
            vertical: "center",
            horizontal: "center"
        },
        border: {
            bottom: {
                style: "thin"
            }
        }
    };
    ws['O6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 13,
            bold: true,
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            }
        }
    };
    ws['M6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        }
    };
    ws['N6'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            }
        }
    };
    ws['O4'].s = { // set the style for target cell
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            bottom: {
                style: "thin"
            },
            right: {
                style: "thin"
            }
        }
    };
    ws['O5'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        border: {
            right: {
                style: "thin"
            }
        }
    };
    ws['Q3'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['Q5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['Q6'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };

    ws['R4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['R5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['S3'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['S5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T3'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            right: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['T5'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }

    ws['P2'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    }
    ws['P4'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['P6'].s = {
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            right: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['M2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
            left: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['N2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['O2'].s = {
        fill: {
            fgColor: {
                rgb: "FFFFFF"
            }
        },
        font: {
            name: "arial",
            sz: 14,
            bold: true,
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        }
    };
    ws['E7'].s = {  
        font: {
            name: "Arial",
            sz: 14,
            bold: true,
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['G7'].s = {
        font: {
            name: "Arial",
            sz: 14,
            bold: true,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
        }
    };
    ws['C19'].s = {
        font: {
            name: "Arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}

// la partie graphique (couleurs..) du champ TD
function ColonneTD(ws,j) {
    ws['C'+j] = { t: 't', v: "        TD    " };
    ws['C'+j].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '19'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }

    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + '20'].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
}
  

//partie dynamique du champ TD (vacs jours féries ...)
function BoucleTD2(ws, j) {
    var inf = WeekDay(new Date(binf),1);
    var sup = WeekDay(new Date(bsup),1);
    var winf = inf.getWeekNumber();
    var ecart = ecartWeek(inf, sup);
    var b=0;
    for (var indice = 'H'.charCodeAt(0); indice <'H'.charCodeAt(0)+ecart; indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre+j] = { t: 't', v: "S"+(winf+b)+" "+JJ_MM(inf)};
        ws[lettre+(j+1)] = { t: 't', v: (1+b)};

        ws[lettre +j].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
        ws[lettre + (j+1)].s = {
            font:{
                bold : true,
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            }
        };
        inf.setDate(inf.getDate()+7);
        b++;
        
    if (vac){
        if (typeof vac == "string") {
            vac = [vac];
            var virgule = ",";
            if (vac.length == 1){
            vac = vac[0].split(virgule);}
            }
            var cpt = 0;
            vac.forEach(item=>{
                var x = new Date(item).getWeekNumber();
                var ecart_vac = x-winf;
                var indice = 'H'.charCodeAt(0)+ecart_vac;
                var lettre = String.fromCharCode(indice);
                ws[lettre + j]= {t:'t', v:"VACANCES"};
                ws[lettre+(j+1)] = {t:'t', v:""};
                cpt++;

                ws[lettre +j].s = {
                    fill: {
                        fgColor: { rgb: "C0DFEF" }
                    },
                    font: {
                        name: "arial",
                        sz: 12,
                        bold: true,
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                }
                ws[lettre +(j+1)].s = {
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                }
               
                var y = 1;
                for (var indice = 'H'.charCodeAt(0)+(ecart_vac+1); indice <'H'.charCodeAt(0)+ecart; indice++) {
                    var lettre = String.fromCharCode(indice);
                    if(cpt > 1){
                    ws[lettre+(j+1)] = { t: 't', v: ((ecart_vac+y)-(cpt-1))};
                }
                else{ws[lettre+(j+1)] = { t: 't', v: (ecart_vac+y)};}
                ws[lettre + (j+1)].s = {
                    font:{
                        bold : true,
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    }
                };               
                y++;
                }
            });
        }
    if (LiFer){
        
        LiFer.forEach(item=>{
            var jour = getJour2(item);
            if (jour != "Dimanche" && jour != "Samedi"){
            var fer = new Date(item).getWeekNumber();
            var ecart_ferie = fer-winf;


            var date = new Date(item) 
            var jourdumois = date.getDate();

            var final = jour +" "+ jourdumois + " férié";

            var indice1 = 'H'.charCodeAt(0)+ecart_ferie;
            var lettre = String.fromCharCode(indice1);

            ws[lettre + (j-1)]= {t:'t', v:final};
            ws[lettre +(j-1)].s = {
                fill: {
                    fgColor: { rgb: "C0DFEF" }
                },
                font: {
                    name: "arial",
                    sz: 12,
                    bold: true,
                },
                alignment: {
                    vertical: "center",
                    horizontal: "center"
                },
                border: {
                    top: {
                        style: "thin"
                    },
                    bottom: {
                        style: "thin"
                    },
                    right: {
                        style: "thin"
                    },
                    left: {
                        style: "thin"
                    }
                }
            };
            }
        });  
    }
    
    };


    ws['A'+(j+1)] = { t: 't', v: "Jour" };
    ws['B'+(j+1)] = { t: 't', v: "Créneau" };
    ws['C'+(j+1)] = { t: 't', v: "Créneau non classique" };
    ws['D'+(j+1)] = { t: 't', v: "Enseignant" };
    ws['E'+(j+1)] = { t: 't', v: "Groupe/Série " };
    ws['F'+(j+1)] = { t: 't', v: "Effectif" };
    ws['G'+(j+1)] = { t: 't', v: "Salle" };



    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + j].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + j].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }

    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (j+1)].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
}
// la partie graphique (couleurs..) du champ TD
function ColonneTP(ws, l) {
    ws['C'+l] = { t: 't', v: "        TP    " };
    ws['C'+l].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}
// la partie graphique (couleurs..) du champ TERRAIN
function ColonneTERRAIN(ws, m) {
    ws['C'+m] = { t: 't', v: "        TERRAIN    " };
    ws['C'+m].s = {
        font: {
            name: "arial",
            sz: 16,
            bold: true,
        },
        fill: {
            fgColor: {
                rgb: "FFFF00"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "right"
        },
        border: {
            top: {
                style: "thin"
            },
            bottom: {
                style: "thin"
            },
        }
    };
}

//partie dynamique du champ TD (vacs jours féries ...)
function boucleTP2(ws, l) {

    var inf = WeekDay(new Date(binf),1);
    var sup = WeekDay(new Date(bsup),1);
    var winf = inf.getWeekNumber();
    var ecart = ecartWeek(inf, sup);
    var b=0;
    for (var indice = 'H'.charCodeAt(0); indice <'H'.charCodeAt(0)+ecart; indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre+l] = { t: 't', v: "S"+(winf+b)+" "+JJ_MM(inf)};
        ws[lettre+(l+1)] = { t: 't', v: (1+b)};

        ws[lettre +l].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
        ws[lettre + (l+1)].s = {
            font:{
                bold : true,
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            }
        };
        inf.setDate(inf.getDate()+7);
        b++;
    };

    ws['A'+(l+1)] = { t: 't', v: "Jour" };
    ws['B'+(l+1)] = { t: 't', v: "Créneau" };
    ws['C'+(l+1)] = { t: 't', v: "Créneau non classique" };
    ws['D'+(l+1)] = { t: 't', v: "Enseignant" };
    ws['E'+(l+1)] = { t: 't', v: "Groupe/Série " };
    ws['F'+(l+1)] = { t: 't', v: "Effectif" };
    ws['G'+(l+1)] = { t: 't', v: "Salle" };



    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + l].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + l].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }

    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre +(l+1)].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }

    if (vac){
        if (typeof vac == "string") {
            vac = [vac];
            var virgule = ",";
            if (vac.length == 1){
            vac = vac[0].split(virgule);}
            }
            var cpt = 0;
            vac.forEach(item=>{
                var x = new Date(item).getWeekNumber();
                var ecart_vac = x-winf;
                var indice = 'H'.charCodeAt(0)+ecart_vac;
                var lettre = String.fromCharCode(indice);
                ws[lettre + l]= {t:'t', v:"VACANCES"};
                ws[lettre+(l+1)] = {t:'t', v:""};
                cpt++;

                ws[lettre +l].s = {
                    fill: {
                        fgColor: { rgb: "C0DFEF" }
                    },
                    font: {
                        name: "arial",
                        sz: 12,
                        bold: true,
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                };
                ws[lettre +(l+1)].s = {
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                }
               
                var y = 1;
                for (var indice = 'H'.charCodeAt(0)+(ecart_vac+1); indice <'H'.charCodeAt(0)+ecart; indice++) {
                    var lettre = String.fromCharCode(indice);
                    if(cpt > 1){
                    ws[lettre+(l+1)] = { t: 't', v: ((ecart_vac+y)-(cpt-1))};
                }
                else{ws[lettre+(l+1)] = { t: 't', v: (ecart_vac+y)};}
                ws[lettre + (l+1)].s = {
                    font:{
                        bold : true,
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    }
                };               
                y++;
                }
            });
        }
    if (LiFer){
        
        LiFer.forEach(item=>{
            var jour = getJour2(item);
            if (jour != "Dimanche" && jour != "Samedi"){
            var fer = new Date(item).getWeekNumber();
            var ecart_ferie = fer-winf;


            var date = new Date(item) 
            var jourdumois = date.getDate();

            var final = jour +" "+ jourdumois + " férié";

            var indice1 = 'H'.charCodeAt(0)+ecart_ferie;
            var lettre = String.fromCharCode(indice1);

            ws[lettre + (l-1)]= {t:'t', v:final};
            ws[lettre +(l-1)].s = {
                fill: {
                    fgColor: { rgb: "C0DFEF" }
                },
                font: {
                    name: "arial",
                    sz: 12,
                    bold: true,
                },
                alignment: {
                    vertical: "center",
                    horizontal: "center"
                },
                border: {
                    top: {
                        style: "thin"
                    },
                    bottom: {
                        style: "thin"
                    },
                    right: {
                        style: "thin"
                    },
                    left: {
                        style: "thin"
                    }
                }
            };
            }
        });  
    }


}
//partie dynamique du champ TD (vacs jours féries ...)
function boucleTERRAIN2(ws, m) {
    var inf = WeekDay(new Date(binf),1);
    var sup = WeekDay(new Date(bsup),1);
    var winf = inf.getWeekNumber();
    var ecart = ecartWeek(inf, sup);
    var b=0;
    for (var indice = 'H'.charCodeAt(0); indice <'H'.charCodeAt(0)+ecart; indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre+m] = { t: 't', v: "S"+(winf+b)+" "+JJ_MM(inf)};
        ws[lettre+(m+1)] = { t: 't', v: (1+b)};

        ws[lettre +m].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }
        };
        ws[lettre + (m+1)].s = {
            font:{
                bold : true,
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            }
        };
        inf.setDate(inf.getDate()+7);
        b++;
    };

    ws['A'+(m+1)] = { t: 't', v: "Jour" };
    ws['B'+(m+1)] = { t: 't', v: "Créneau" };
    ws['C'+(m+1)] = { t: 't', v: "Créneau non classique" };
    ws['D'+(m+1)] = { t: 't', v: "Enseignant" };
    ws['E'+(m+1)] = { t: 't', v: "Groupe/Série " };
    ws['F'+(m+1)] = { t: 't', v: "Effectif" };
    ws['G'+(m+1)] = { t: 't', v: "Salle" };



    for (var indice = 'A'.charCodeAt(0); indice <= 'B'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + m].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }
        };
    }
    for (var indice = 'D'.charCodeAt(0); indice < 'H'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + m].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },

            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                }
            }

        };
    }

    for (var indice = 'A'.charCodeAt(0); indice <= 'G'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
        ws[lettre + (m+1)].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
                name: "arial",
                sz: 12,
                bold: true,
            },
            alignment: {
                vertical: "center",
                horizontal: "center"
            },
            border: {
                top: {
                    style: "thin"
                },
                bottom: {
                    style: "thin"
                },
                right: {
                    style: "thin"
                },
                left: {
                    style: "thin"
                }
            }

        };
    }
    if (vac){
        if (typeof vac == "string") {
            vac = [vac];
            var virgule = ",";
            if (vac.length == 1){
            vac = vac[0].split(virgule);}
            }
            var cpt = 0;
            vac.forEach(item=>{
                var x = new Date(item).getWeekNumber();
                var ecart_vac = x-winf;
                var indice = 'H'.charCodeAt(0)+ecart_vac;
                var lettre = String.fromCharCode(indice);
                ws[lettre + m]= {t:'t', v:"VACANCES"};
                ws[lettre+(m+1)] = {t:'t', v:""};
                cpt++;

                ws[lettre +m].s = {
                    fill: {
                        fgColor: { rgb: "C0DFEF" }
                    },
                    font: {
                        name: "arial",
                        sz: 12,
                        bold: true,
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                };
                ws[lettre +(m+1)].s = {
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                };
               
                var y = 1;
                for (var indice = 'H'.charCodeAt(0)+(ecart_vac+1); indice <'H'.charCodeAt(0)+ecart; indice++) {
                    var lettre = String.fromCharCode(indice);
                    if(cpt > 1){
                    ws[lettre+(m+1)] = { t: 't', v: ((ecart_vac+y)-(cpt-1))};
                }
                else{ws[lettre+(m+1)] = { t: 't', v: (ecart_vac+y)};}
                ws[lettre + (m+1)].s = {
                    font:{
                        bold : true,
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    }
                };               
                y++;
                }
            });
    }
    if (LiFer){
        
        LiFer.forEach(item=>{
            var jour = getJour2(item);
            if (jour != "Dimanche" && jour != "Samedi"){
            var fer = new Date(item).getWeekNumber();
            var ecart_ferie = fer-winf;


            var date = new Date(item) 
            var jourdumois = date.getDate();

            var final = jour +" "+ jourdumois + " férié";

            var indice1 = 'H'.charCodeAt(0)+ecart_ferie;
            var lettre = String.fromCharCode(indice1);

            ws[lettre + (m-1)]= {t:'t', v:final};
            ws[lettre +(m-1)].s = {
                fill: {
                    fgColor: { rgb: "C0DFEF" }
                },
                font: {
                    name: "arial",
                    sz: 12,
                    bold: true,
                },
                alignment: {
                    vertical: "center",
                    horizontal: "center"
                },
                border: {
                    top: {
                        style: "thin"
                    },
                    bottom: {
                        style: "thin"
                    },
                    right: {
                        style: "thin"
                    },
                    left: {
                        style: "thin"
                    }
                }
            };
            }
        });  
    }
    

}
// partie dynamique du petit tableau en haut à droite du nombre de CM, TD,TP
function QRS(ws){
    for (var indice = 'Q'.charCodeAt(0); indice < 'T'.charCodeAt(0); indice++) {
        var lettre = String.fromCharCode(indice);
    ws[lettre + '3'].s = {
        font: {
            name : "arial",
            sz: 14,
            bold : true,
            color: {
                rgb: "FF0000"
            }
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            },
        border: {
            bottom: {
                style: "thin"
                },
            right: {
                style: "thin"
                },
            left: {
                style: "thin"
                }
            }
         };
         ws[lettre + '4'].s = {
            font: {
                name : "arial",
                sz: 14,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                },
            border: {
                bottom: {
                    style: "thin"
                    },
                right: {
                    style: "thin"
                    },
                left: {
                    style: "thin"
                    }
                }
             };
    }
    ws['S5'].s = {
        font: {
            name : "arial",
            sz: 14,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            },
        border: {
            bottom: {
                style: "thin"
                },
            right: {
                style: "thin"
                },
            left: {
                style: "thin"
                }
            }
         };
         ws['Q6'].s = {
            font: {
                name : "arial",
                sz: 14,
                bold : true,
                color: {
                    rgb: "FF0000"
                }
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                },
            border: {
                bottom: {
                    style: "thin"
                    },
                right: {
                    style: "thin"
                    },
                left: {
                    style: "thin"
                    }
                }
             };
}
//feuille UE de la mention
function UEdelaMention(ws){
    ws['A1'] = { t: 't', v: " Code UE " };
    ws['B1'] = { t: 't', v: " Nom UE" };
    ws['A1'].s = {   
        fill: {
            fgColor: { rgb: "FFFFF00" }
        },
        font: {
          name : "Verdana",
          sz: 12,
          bold : true,
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "medium",
                color: "000000"
            },
            top :{
                style : "medium",
                color: "000000"
            },
            bottom :{
                style : "medium",
                color: "000000"
            }
        }
      };
    ws['B1'].s = {
        fill: {
            fgColor: { rgb: "FFFFF00" }
        },
        font: {
          name : "Verdana",  
          sz: 12,
          bold : true
        },
        alignment: {
            vertical: "center",
            horizontal: "center",
            wrapText: '1',
        },
        border: {
            right: {
                style: "medium",
                color: "000000"
            },
            left: {
                style: "thin",
                color: "000000"
            },
            top :{
                style : "medium",
                color: "000000"

            },
            bottom :{
                style : "medium",
                color: "000000"
            }
        }
      };
    var contenuJSON = recupJSON();
    var y = []; 
    var x = [];
    contenuJSON.forEach(Objet => {
        y.push(Objet['Code UE']);
      });
    contenuJSON.forEach(Objet => {
        x.push(Objet['Libellé']);
      });
    for (var indice=0;indice<y.length;indice++){
        ws['A'+(indice+2)]= {t: 't', v: y[indice]};
        ws['B'+(indice+2)]= {t: 't', v: x[indice]};
      
        ws['A' + (indice+2)].s = {   
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
              name : "Verdana",
              sz: 12,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "medium",
                    color: "000000"
                },
                left: {
                    style: "medium",
                    color: "000000"
                },
                top :{
                    style : "medium",
                    color: "000000"
                },
                bottom :{
                    style : "medium",
                    color: "000000"
                }
            }
          };
        ws['B' + (indice+2)].s = {
            fill: {
                fgColor: { rgb: "FFFFF00" }
            },
            font: {
              name : "Verdana",  
              sz: 12,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "medium",
                    color: "000000"
                },
                top :{
                    style : "medium",
                    color: "000000"

                },
                bottom :{
                    style : "medium",
                    color: "000000"
                }
            }
          };
        }
}
//filtre de la liste des créneaux par UE
function filtreUE(codeUE, liste) {
	var res = [];
	liste.forEach(el => {
		if (el.extendedProps.CodeUE == codeUE) {
			res.push(el);
		}
	});
    return res;
};
//filtre de la liste des créneaux par type (CM,TD,TP)
function filtretype(type, liste) {
	var res = [];
	liste.forEach(el => {
		if (el.extendedProps.Type == type) {
			res.push(el);
		}
	});
	return res;
};
// fonction qu'on lui passe un créneau de l'année et elle donne le jour corresponsant (dimanche, lundi, mardi...)
function getJour(crenaux) {
	x = new Date(crenaux.start).getDay();
	switch (x) {
		case (1):
			return "Lundi";
			break;
		case (2):
			return "Mardi";
			break;
		case (3):
			return "Mercredi";
			break;
		case (4):
			return "Jeudi";
			break;
		case (5):
			return "Vendredi";
			break;
	}
};
// fonction qu'on lui passe un créneau en forme de DATE et elle donne le jour corresponsant (dimanche, lundi, mardi...)
function getJour2(date){
    var d = new Date(date);
    var x = d.getDay(); 
    switch (x) {
        case (1):
            return "Lundi";
            break;
        case (2):
            return "Mardi";
            break;
        case (3):
            return "Mercredi";
            break;
        case (4):
            return "Jeudi";
            break;
        case (5):
            return "Vendredi";
            break;
        case (6):
            return "Samedi";
            break;
        case (0):
            return "Dimanche";
            break;

    }
    }
// fonction qui rajoute un 0 au début de la date par ex 9 aout elle met 09 aout 
function pad(number) {
	if (number < 10) {
		return '0' + number;
	}
	return number;
};
// pour écrire les créneaux sous format ** h ** - ** h **
function getdure(crenaux) {
	ah = pad(new Date(crenaux.start).getHours()).toString();
	am = pad(new Date(crenaux.start).getMinutes()).toString();
	bh = pad(new Date(crenaux.end).getHours()).toString();
	bm = pad(new Date(crenaux.end).getMinutes()).toString();
	return ah + "h" + am + "-" + bh + "h" + bm;
};

//retourne un tableau du jour, créneau sous forme ** h ** - ** h **, et le groupe
function format_DJG(crenaux) {
	return [getJour(crenaux), getdure(crenaux), crenaux.extendedProps.Groupe,[],[],[]];

}

//GENERALISATION des fonctions au dessus : en arguement on donne un indice de 0 a 6 et une date (dimanche a lundi et cela va nous retourner la date correspondant au jour de la semaine)
function WeekDay(date, i) {
	if (date.getDay() == 0) { // si on est dimanche, je reviens une semaine en arriere avant de lire le jour suivant
		return (new Date(date.setDate(date.getDate() - 7 + i)));
	}
	return new Date(date.setDate(date.getDate() - date.getDay() + i));
};
// on en aura besoin pour écrire la date du lundi de la semaine par ex S1 01/01
function JJ_MM(date){
	return pad(date.getUTCDate().toString())+'/'+pad((date.getMonth() + 1 ).toString());
    
}
// fonction trouvée sur internet qui retourne le numéro d'une semaine par rapport à l'année par exemple la semaine du 07/01 c'est la semaine 2
Date.prototype.getWeekNumber = function () {
	var oneJan = new Date(this.getFullYear(), 0, 1);
	var numberOfDays = Math.floor((this - oneJan) / (24 * 60 * 60 * 1000));
	var fix = 1;
	if (this.getDay() == 1 || this.getDay() == 2 ) {
	  fix = 0;
	}
	return Math.ceil((this.getDay() + 1 + numberOfDays) / 7) - fix;
  };

// la liste finale des créneaux d'une UE par type (CM par ex) écrite sous la forme DJG vue au dessus
function convertall_DJG(liste) {
	var x = []; // Tableau resultat
	liste.forEach(el => { // pour tout les elements de la liste qu'on donne (liste Cm/Td/Tp)
		var y = 0; // Verification si un element existe déja dans x  
		var z = format_DJG(el); // l'element actuellement étudié, c'est un crénau
        var weeknb = new Date(el.start).getWeekNumber(); // la semaine du crénau étudié
        var inform = el.extendedProps.Informatise;
        var interv = el.extendedProps.Intervenant;
        z[5] = interv;

		x.forEach(ele => { // Pour tout les elements du tableau resultat
			if (ele[0] == z[0]) { // Si le jour de la semaine est le meme 
				if (ele[1] == z[1]) { // Si la durée est la même
					if (ele[2] == z[2]) { // Si le groupe est le même
						y = 1; // Alors il existait déja et donc pas besoin de le rajouter
                        ele[3].push(weeknb);
                        ele[4].push(inform);
                        if(interv != ""){ele[5].push(interv)};
					}
				}
			}
		});
		if (y == 0) { // Si il n'existait pas   
            var add = format_DJG(el); // On le créer
            add[3].push(weeknb);// On y update sa weeknumber
            add[4].push(inform);
            if(interv!=""){add[5].push(interv)};  
			x.push(add); // On l'ajoute
		}; 
        });
        return x;
};
// la différence entre 2 semaine
function ecartWeek(inf,sup){
	  return Math.abs(new Date(sup).getWeekNumber() - new Date(inf).getWeekNumber())+1;
  }
// c'est là où on va commencer à écrire
function convertJsonToExcel() {
    //créer un fichier excel
    const workBook = xlsx.utils.book_new();
    // notre fichier json là où on récupère notre liste d'UE dynamique
    var contenuJSON = recupJSON();
    var y = [];

    var CM = new Object;
    var TD = new Object;
    var TP = new Object;
    var NomUE = new Object;
    var Effectif = new Object;

    contenuJSON.forEach(Objet => {
        y.push(Objet['Code UE']);
        CM[Objet['Code UE']]= Objet.CM;
        TD[Objet['Code UE']]= Objet.TD;
        TP[Objet['Code UE']]= Objet.TP;
        NomUE[Objet['Code UE']]= Objet['Libellé'];
        Effectif[Objet['Code UE']]= Objet['Nbre d\'inscrits'];
    });

    // y contient la liste des codeUE - array
    // CM contient la liste des h de CM - dico
    // TD contient la liste des h de TD - dico
    // TP contient la liste des h de TP - dico 
  
       // Je crée une feuille vide par UE
       var uneligne = [];
       for (let i = 1; i < 100; i++)
           uneligne.push("");
       var table = [];
       for (let i = 1; i < 100; i++)
           table.push(uneligne);

    for (var key in y) {
        var ws = xlsx.utils.aoa_to_sheet(table);
        xlsx.utils.book_append_sheet(workBook, ws, y[key]);
        // qlqs éléments dynamiques qu'on ajoute, le code de l'UE dans certaines cases, le nombre de TD, TP, CM dans le tableau en haut ...
        ws['D3'] = { t: 't', v: y[key] };
        ws['D4'] = {t : 't', v: NomUE[y[key]]};
        ws['Q3'] = { t: 't', v: CM[y[key]] };
        ws['R3'] = { t: 't', v: TD[y[key]] };
        ws['S3'] = { t: 't', v: TP[y[key]] };
        ws['Q4'] = { t: 't', v: CM[y[key]]/1.5 };
        ws['R4'] = { t: 't', v: TD[y[key]]/1.5 };
        ws['S4'] = { t: 't', v: TP[y[key]]/1.5 };
        ws['Q6'] = { t: 't', v: Effectif[y[key]]};

        if (!TD[y[key]]){ws['R3']={ t: 't', v: '0' }; ws['R4']={ t: 't', v: '0' }}
        if (!TP[y[key]]){ws['S3']={ t: 't', v: '0' }; ws['S4']={ t: 't', v: '0' };ws['S5']={ t: 't', v: '0' }}
        if (TP[y[key]] &&TP[y[key]]%3==0) {ws['S5']={t: 't', v:TP[y[key]]/3}}
        else {ws['S5']={t: 't', v:'0'}}
        //appliquer les filtrages écrits au dessus pour la liste des UE qu'on récupère du fichier json
        var LiCrenUE = filtreUE(y[key],LiCren);
        var LiCrenUE_CM = filtretype('CM', LiCrenUE);
        var LiCrenUE_TP  = filtretype('TP', LiCrenUE);
        var LiCrenUE_TD = filtretype('TD', LiCrenUE);
        var LiCrenUE_CM_DJG = convertall_DJG(LiCrenUE_CM);
        var LiCrenUE_TD_DJG = convertall_DJG(LiCrenUE_TD);
        var LiCrenUE_TP_DJG = convertall_DJG(LiCrenUE_TP);
        
        // écrire les intervenants avec les initiales
        var listeall_interv = [];
        LiCrenUE.forEach(el=>{
            var y = 0;
            var x = el.extendedProps.Intervenant;
            listeall_interv.forEach(ele=>{
                if (ele == x){
                    y = 1;
                }
            });
            if ((y == 0)&& (x!="")){
                listeall_interv.push(x);
            }
        }) 
        var initial = "";
        var ii= 0;
        listeall_interv.forEach(el=>{
        var tab = el.split(" ");
        initial = tab[0].substr(0,1)+tab[1].substr(0,1);
        
        ws['D'+(ii+11)]={t:'t', v:initial};
        ws['E'+(ii+11)]={t:'t', v:el};
        ws['E' + (ii+11)].s = {
            font: {
                name: "Verdana",
                sz: 12,
            },
            alignment: {
                vertical: "center",
                horizontal: "center",
                wrapText: '1',
            },
            border: {
                right: {
                    style: "thin",
                    color: "000000"
                },
                left: {
                    style: "medium",
                    color: "000000"
                },
                top: {
                    style: "thin",
                    color: "000000"
                },
                bottom: {
                    style: "thin",
                    color: "000000"
                }
            }
        }; ii++;
    }); 

    // la partie dynamique des CM
        var inf = WeekDay(new Date(binf),1);
        var sup = WeekDay(new Date(bsup),1);
        var winf = inf.getWeekNumber();
        var wsup = sup.getWeekNumber();
        var ecart = ecartWeek(inf, sup);
    // écrire les jours féries
    if (LiFer){
        LiFer.forEach(item=>{
            var jour = getJour2(item);
            if (jour != "Dimanche" && jour != "Samedi"){
            var fer = new Date(item).getWeekNumber();
            var ecart_ferie = fer-winf;


            var date = new Date(item) 
            var jourdumois = date.getDate();

            var final = jour +" "+ jourdumois + " férié";

            var indice1 = 'H'.charCodeAt(0)+ecart_ferie;
            var lettre = String.fromCharCode(indice1);

            ws[lettre + 18]= {t:'t', v:final};
            ws[lettre +18].s = {
                fill: {
                    fgColor: { rgb: "C0DFEF" }
                },
                font: {
                    name: "arial",
                    sz: 12,
                    bold: true,
                },
                alignment: {
                    vertical: "center",
                    horizontal: "center"
                },
                border: {
                    top: {
                        style: "thin"
                    },
                    bottom: {
                        style: "thin"
                    },
                    right: {
                        style: "thin"
                    },
                    left: {
                        style: "thin"
                    }
                }
            };
            }
        });    
    }
    // le tableau dynamique des semaines du champ CM
        var b=0;
            for (var indice = 'H'.charCodeAt(0); indice <'H'.charCodeAt(0)+ecart; indice++) {
                var lettre = String.fromCharCode(indice);
                ws[lettre+19] = { t: 't', v: "S"+(winf+b)+" "+JJ_MM(inf)};
                ws[lettre+20] = { t: 't', v: (1+b)};

                ws[lettre +19].s = {
                    fill: {
                        fgColor: { rgb: "FFFFF00" }
                    },
                    font: {
                        name: "arial",
                        sz: 12,
                        bold: true,
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    },
                    border: {
                        top: {
                            style: "thin" 
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                };
                ws[lettre + '20'].s = {
                    font:{
                        bold : true,
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    }
                };
                inf.setDate(inf.getDate()+7);
                b++;
            }
        //gérer les vacances pour le tableau CM
        if (vac){
        if (typeof vac == "string") {
            vac = [vac];
            var virgule = ",";
            if (vac.length == 1){
            vac = vac[0].split(virgule);}
            }
            var cpt = 0;
            vac.forEach(item=>{
                var x = new Date(item).getWeekNumber();
                var ecart_vac = x-winf;
                var indice = 'H'.charCodeAt(0)+ecart_vac;
                var lettre = String.fromCharCode(indice);
                ws[lettre + 19]= {t:'t', v:"VACANCES"};
                ws[lettre+20] = {t:'t', v:""};
                cpt++;

                ws[lettre +19].s = {
                    fill: {
                        fgColor: { rgb: "C0DFEF" }
                    },
                    font: {
                        name: "arial",
                        sz: 12,
                        bold: true,
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                };
                ws[lettre +20].s = {
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    }
                }
               
                var y = 1;
                for (var indice = 'H'.charCodeAt(0)+(ecart_vac+1); indice <'H'.charCodeAt(0)+ecart; indice++) {
                    var lettre = String.fromCharCode(indice);
                    if(cpt > 1){
                    ws[lettre+20] = { t: 't', v: ((ecart_vac+y)-(cpt-1))};
                }
                else{ws[lettre+20] = { t: 't', v: (ecart_vac+y)};}
                ws[lettre + 20].s = {
                    font:{
                        bold : true,
                    },
                    border: {
                        top: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        }
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center"
                    }
                };               
                y++;
                }
            });
        }
        //écrire le jour, le créneau et le groupe des CM
        var i=0;
        LiCrenUE_CM_DJG.forEach(el=>{
            ws['A'+(i+21)]={t: 't', v:el[0]};
            ws['B'+(i+21)]= {t: 't', v:el[1]};
            ws['E'+(i+21)]= {t: 't', v:el[2]};
            el[3].forEach(ele=>{
                var index = el[3].indexOf(ele);
                var ecart_croix = ele - winf;
                var indice = 'H'.charCodeAt(0)+ecart_croix;
                var lettre = String.fromCharCode(indice);
                if (el[4][index]){
                    ws[lettre+(i+21)]= {t: 't', v:"I"};
                }
                else {ws[lettre+(i+21)]= {t: 't', v:"x"};}
                var interv_SD = [...new Set(el[5])]
                var initial = "";
                var tab_init = [];

                interv_SD.forEach(item=>{
                var tab = item.split(" ");
                initial = tab[0].substr(0,1)+tab[1].substr(0,1);
                tab_init.push(initial);
                })
                var tab_final = tab_init.toString();
                ws['D'+(i+21)]={t:'t',v:tab_final.replaceAll(','," - ")};                
            })


            for (var indice = 'A'.charCodeAt(0); indice < 'H'.charCodeAt(0)+ecart; indice++) {
                var lettre = String.fromCharCode(indice);
                ws[lettre + (i+21)].s = {
                    border: {
                        top: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        }
                    },
                    font: {
                        name : "Arial",
                        sz: 14,
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center",
                        wrapText: '1', 
                    },
                };
            }i++;
        });
        //écrire le jour, le créneau et le groupe des TD
        var j = 21+(i+2);
        var k = 21 +(i+4);
        var e=0;
        LiCrenUE_TD_DJG.forEach(el=>{
            ws['A'+(k+e)]={t: 't', v:el[0]};
            ws['B'+(k+e)]= {t: 't', v:el[1]};
            ws['E'+(k+e)]= {t: 't', v:el[2]};
            el[3].forEach(ele=>{
                var index = el[3].indexOf(ele);
                var ecart_croix = ele - winf;
                var indice = 'H'.charCodeAt(0)+ecart_croix;
                var lettre = String.fromCharCode(indice);
                if (el[4][index]){
                    ws[lettre+(k+e)]= {t: 't', v:"I"};
                }
                else {ws[lettre+(k+e)]= {t: 't', v:"x"};}
                var interv_SD = [...new Set(el[5])]
                var initial = "";
                var tab_init = [];

                interv_SD.forEach(item=>{
                var tab = item.split(" ");
                initial = tab[0].substr(0,1)+tab[1].substr(0,1);
                tab_init.push(initial);
                })
                var tab_final = tab_init.toString();
                ws['D'+(k+e)]={t:'t',v:tab_final.replaceAll(','," - ")};                
            })


            for (var indice = 'A'.charCodeAt(0); indice < 'H'.charCodeAt(0)+ecart; indice++) {
                var lettre = String.fromCharCode(indice);
                ws[lettre + (k+e)].s = {
                    border: {
                        top: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        }
                    },
                    font: {
                        name : "Arial",
                        sz: 14,
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center",
                        wrapText: '1', 
                    },
                };
            }e++;
        });
    //écrire le jour, le créneau et le groupe des TP
        var s=0;             
        var l = k +(e+2);
        var h = k +(e+4);

        LiCrenUE_TP_DJG.forEach(el=>{
            ws['A'+(h+s)]={t: 't', v:el[0]};
            ws['B'+(h+s)]= {t: 't', v:el[1]};
            ws['E'+(h+s)]= {t: 't', v:el[2]};
            el[3].forEach(ele=>{
                var index = el[3].indexOf(ele);
                var ecart_croix = ele - winf;
                var indice = 'H'.charCodeAt(0)+ecart_croix;
                var lettre = String.fromCharCode(indice);
                if (el[4][index]){
                    ws[lettre+(h+s)]= {t: 't', v:"I"};
                }
                else {ws[lettre+(h+s)]= {t: 't', v:"x"};}
                var interv_SD = [...new Set(el[5])]
                var initial = "";
                var tab_init = [];

                interv_SD.forEach(item=>{
                var tab = item.split(" ");
                initial = tab[0].substr(0,1)+tab[1].substr(0,1);
                tab_init.push(initial);
                })
                var tab_final = tab_init.toString();
                ws['D'+(h+s)]={t:'t',v:tab_final.replaceAll(','," - ")};                
            })


            for (var indice = 'A'.charCodeAt(0); indice < 'H'.charCodeAt(0)+ecart; indice++) {
                var lettre = String.fromCharCode(indice);
                ws[lettre + (h+s)].s = {
                    border: {
                        top: {
                            style: "thin"
                        },
                        right: {
                            style: "thin"
                        },
                        left: {
                            style: "thin"
                        },
                        bottom: {
                            style: "thin"
                        }
                    },
                    font: {
                        name : "Arial",
                        sz: 14,
                    },
                    alignment: {
                        vertical: "center",
                        horizontal: "center",
                        wrapText: '1', 
                    },
                };
            }s++;
        });
        var m = h +(s+2);
        //appeler toutes les fonctions
        Statique(ws);
        BoucleTD2(ws,j);
        ColonneTD(ws, j);
        ColonneTP(ws, l);
        ColonneTERRAIN(ws, m);
        boucleTP2(ws, l);
        boucleTERRAIN2(ws, m);
        QRS(ws);
        
        //largeur et épaisseur des cellules des feuilles des UEs
        ws['!cols'] = [{ width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 7 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }]; //set col. widths
        ws['!rows'] =  [{ hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, 
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
                        { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 }];
        
        //merger des cellules
            const merge = [
            { s: { r: 0, c: 3 }, e: { r: 0, c: 11 }},
            { s: { r: 1, c: 3 }, e: { r: 1, c: 11 }},
            { s: { r: 2, c: 3 }, e: { r: 2, c: 11 }},
            { s: { r: 3, c: 3 }, e: { r: 3, c: 11 }},
            { s: { r: 6, c: 4 }, e: { r: 6, c: 5 }},
            { s: { r: 6, c: 6 }, e: { r: 6, c: 23 }},
            { s: { r: 5, c: 17 }, e: { r: 5, c: 23 }}
          ];
          ws["!merges"] = merge;
    }
    //créer la feuille UE de la mention et la remplir
    var ws2 = xlsx.utils.aoa_to_sheet(table);
    xlsx.utils.book_append_sheet(workBook, ws2, "UE de la mention");
    UEdelaMention(ws2);
    //largeur et épaisseur des cellules de la feuille UE de la mention
    ws2['!cols'] = [{ width: 18 }, { width: 50 }, { width: 18 }, { width: 18 }, { width: 18 }, { width: 18 }]; //set col. widths
    ws2['!rows'] = [{ hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, // set row. heights
    { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
    { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
    { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 },
    { hpx: 30 }, { hpx: 30 }, { hpx: 30 }, { hpx: 30 }];
    xlsx.writeFile(workBook, "Trame_finale.xlsx");
    XLSX.writeFile(workBook, 'Trame_finale.xlsx');
}
convertJsonToExcel();
}
exports.generateExcel = generateExcel;
