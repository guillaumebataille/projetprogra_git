var XLSX = require("xlsx");
var fs = require("fs");

//Reading the Excel File
const wb = XLSX.readFile("./Trame.ods");
//const wb = XLSX.readFile("./Trame.ods");

// Read a specific sheet from the workbook
const ws = wb.Sheets["UE FdS"];
//console.log(ws);
//console.log(wb.SheetNames);

//Read the sheet data and conversion to JSON
const data = XLSX.utils.sheet_to_json(ws);
//console.log(data);

//Removing the doublons
let t = [];
let y;
data.forEach((en) => {
  y = 0;
  t.forEach((el) => {
    if (en["Code UE"] == el["Code UE"]) {
      y = 1;
    }
  });
  if (y == 0) {
    t.push(en);
  }
});

var z = "HAI1";

function filtreUE(UEfi, uniqueUE) {
  console.log("from Import uniqueUE: ", uniqueUE);
  return t.filter((x) => x["Code UE"].slice(0, 4) == z);
}

fs.writeFileSync("./importUE.json", JSON.stringify(filtreUE(z), null, 2));
