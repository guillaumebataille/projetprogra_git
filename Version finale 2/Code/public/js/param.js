//const { Calendar } = require("@fullcalendar/core");

console.log("In param.js");

//Variable utile
function recupallevent(calendar) {
  return calendar.getEvents();
}

let plageinf = new Date(2021, 08, 01);
let plagesup = new Date(2022, 05, 01); //mois -1

function toY_M_D(item) {
  return (
    item.getFullYear().toString() +
    "-" +
    pad(item.getMonth() + 1).toString() +
    "-" +
    pad(item.getDate()).toString()
  );
}

function Mon_Week() {
  var date = new Date();
  if (date.getDay() == 0) {
    // si on est dimanche, je reviens une semaine en arriere
    return new Date(date.setDate(date.getDate() - 7 + 1));
  }
  return new Date(date.setDate(date.getDate() - date.getDay() + 1));
}

function Sat_Week() {
  var date = new Date();
  if (date.getDay() == 0) {
    // si on est dimanche, je reviens une semaine en arriere
    return new Date(date.setDate(date.getDate() - 7 + 6));
  }
  return new Date(date.setDate(date.getDate() - date.getDay() + 6));
}

function option_classic(liCodeUe, LiUE, dateinf, datesup, vac) {
  return {
    eventClick: function (info) {
      var i = 0;
      var border = info.el.style.borderColor; // SAuvegarde de la bordure
      info.el.style.borderColor = "red"; // Selection visuelle de l'ue en mettant en surbrillance l'event
      document.getElementById("popup-Form").style.display = "block"; // Affichage du bloc
      let input1 = document.getElementById("ID_input1"); // Recuperation du champ de texte
      let input2 = document.getElementsByName("ID_input2")[0]; // Recuperation d'un radio
      let bouton1 = document.getElementById("ID_bouton1"); // Recuperation du bouton modifier
      let bouton2 = document.getElementById("ID_bouton2"); // Recuperation du bouton supprimer
      bouton1.addEventListener("click", click_bouton1); // Listener sur le premier bouton
      bouton2.addEventListener("click", click_bouton2); // Listener sur le deuxieme bouton

      function click_bouton2() {
        while (i == 0) {
          info.event.remove(); // Supression de l'event courant
          var current_UE;
          var current_grp = info.event._def.extendedProps.Groupe;
          var current_type = info.event._def.extendedProps.Type;
          console.log(current_grp, current_type);
          console.log(info.event);
          LiUE.forEach((UE) => {
            if (UE.CodeUE == getcodeUE(info.event)) current_UE = UE; // Recuperation de L'UE courant
          });
          current_UE.retraitbloc(current_type, current_grp); // On redonne les CM pris
          reloadEvents(liCodeUe, LiUE);
          document.getElementById("popup-Form").style.display = "none"; // On retire le formulaire
          i++;
        }
      }

      function click_bouton1() {
        while (i == 0) {
          var Informatisé;
          var Intervenant = input1.value;
          console.log("valeur eventid:", Intervenant);
          console.log("dans event ", info.event._def.extendedProps);
          if (input2.checked) {
            Informatisé = 0;
          } else {
            Informatisé = 1;
          }

          info.event.setExtendedProp("Informatise", Informatisé);
          info.event.setExtendedProp("Intervenant", Intervenant);

          //info.event._def.extendedProps.Intervenant = Inter;
          console.log("here", info);
          console.log(
            "Intervenant : ",
            info.event._def.extendedProps.Intervenant
          );
          console.log("Infor : ", info.event._def.extendedProps.Informatise);
          /*  document.getElementById("ID_input1").style.display = "none";
        document.getElementById("ID_bouton1").style.display = "none";
        document.getElementsByName("ID_input2")[0].style.display = "none";
        document.getElementsByName("ID_input2")[1].style.display = "none";*/
          document.getElementById("popup-Form").style.display = "none";
          info.el.style.borderColor = border;
          i++;
        }
      }

      document
        .querySelector('[name="myForm"]')
        .addEventListener("submit", function (event) {
          event.preventDefault();
          //console.log(event);
        });

      //console.log(info.event);
    },
    initialView: "timeGridWeek",
    headerToolbar: {
      left: "prev",
      center: "title",
      end: "next",
    },
    buttonIcons: {
      prev: "chevrons-left",
      next: "chevrons-right",
    },
    views: {
      timeGridWeek: {
        eventMinHeight: 15,
        allDaySlot: false,
        slotMinTime: "08:00:00",
        slotMaxTime: "20:00:00",
        slotDuration: "00:30:00",
        expandRows: true,
        slotEventOverlap: false,
      },
    },
    weekends: false,
    eventDurationEditable: false,
    initialDate: "2021-09-01",
    validRange: {
      start: dateinf,
      end: datesup,
    },
    locale: "fr",
    editable: true,
    droppable: true,
    weekNumbers: true,
    eventOrder: "Type, Groupe",
    eventOverlap: function (stillEvent, movingEvent) {
      console.log("Event immobile :", stillEvent);
      console.log("Event mobile :", movingEvent);
      var sameInterv =
        stillEvent._def.extendedProps.Intervenant !=
        movingEvent._def.extendedProps.Intervenant;
      if (
        stillEvent._def.extendedProps.Intervenant == "" &&
        movingEvent._def.extendedProps.Intervenant == ""
      ) {
        sameInterv = 1;
      }
      var cmboth =
        stillEvent._def.extendedProps.Type == "CM" ||
        movingEvent._def.extendedProps.Type == "CM";
      var sameGroupe =
        stillEvent._def.extendedProps.Groupe !=
        movingEvent._def.extendedProps.Groupe;
      var CodeStill = stillEvent._def.extendedProps.CodeUE;
      var CodeMobile = movingEvent._def.extendedProps.CodeUE;
      console.log(CodeStill.slice(0, 3), CodeStill.slice(4, 7));
      var boolAnglais1 = 1;
      var boolAnglais2 = 1;
      if (CodeStill.slice(0, 3) == "HAL" && CodeStill.slice(4, 7) == "01L") {
        boolAnglais1 = 0;
      }
      if (CodeMobile.slice(0, 3) == "HAL" && CodeMobile.slice(4, 7) == "01L") {
        boolAnglais2 = 0;
      }
      var boolAnglais = boolAnglais1 == boolAnglais2;
      console.log("drop?", sameInterv, sameGroupe);
      return sameInterv && sameGroupe && boolAnglais && !cmboth;
    },
    drop: function (info) {
      uecheck = LiUE[info.draggedEl.id];
      wichCourse = course(info.draggedEl.childNodes[0].childNodes[0].innerText);
      switch (wichCourse) {
        case "CM":
          uecheck.ajoutCM();
          reloadEvents(liCodeUe, LiUE);
          break;

        case "TD":
          var grp = info.draggedEl.childNodes[0].childNodes[0].innerText
            .substr(3)
            .split(" ")[0];
          uecheck.ajoutTDGroupe(grp);
          reloadEvents(liCodeUe, LiUE);
          break;

        case "TP":
          var grp = info.draggedEl.childNodes[0].childNodes[0].innerText
            .substr(3)
            .split(" ")[0];
          uecheck.ajoutTPGroupe(grp);
          reloadEvents(liCodeUe, LiUE);
          break;

        default:
          alert("Erreur!");
          break;
      }
    },
    eventContent: (arg) => {
      if (!ferie(arg.event) && !vacance(arg.event)) {
        return (
          arg.event.title.split(" ")[0] +
          " " +
          getcodeUE(arg.event) +
          " " +
          getduree(arg.event) +
          " " +
          arg.event._def.extendedProps.Intervenant +
          " " +
          (arg.event._def.extendedProps.Informatise ? "| Info" : "")
        );
      }
    },
  };
}

function option_type(liCodeUe, LiUE, cal) {
  return {
    eventClick: function (info) {
      var i = 0;
      var border = info.el.style.borderColor; // SAuvegarde de la bordure
      info.el.style.borderColor = "red"; // Selection visuelle de l'ue en mettant en surbrillance l'event
      document.getElementById("popup-Form").style.display = "block"; // Affichage du bloc
      let input1 = document.getElementById("ID_input1"); // Recuperation du champ de texte
      let input2 = document.getElementsByName("ID_input2")[0]; // Recuperation d'un radio
      let bouton1 = document.getElementById("ID_bouton1"); // Recuperation du bouton modifier
      let bouton2 = document.getElementById("ID_bouton2"); // Recuperation du bouton supprimer
      bouton1.addEventListener("click", click_bouton1); // Listener sur le premier bouton
      bouton2.addEventListener("click", click_bouton2); // Listener sur le deuxieme bouton

      function click_bouton2() {
        while (i == 0) {
          info.event.remove(); // Supression de l'event courant
          var current_UE;
          var current_grp = info.event._def.extendedProps.Groupe;
          var current_type = info.event._def.extendedProps.Type;
          console.log(current_grp, current_type);
          console.log(info.event);
          LiUE.forEach((UE) => {
            if (UE.CodeUE == getcodeUE(info.event)) current_UE = UE; // Recuperation de L'UE courant
          });
          current_UE.retraitbloc(current_type, current_grp); // On redonne les CM pris
          reloadEvents(liCodeUe, LiUE);
          document.getElementById("popup-Form").style.display = "none"; // On retire le formulaire
          i++;
        }
      }

      function click_bouton1() {
        while (i == 0) {
          var Informatisé;
          var Intervenant = input1.value;
          console.log("valeur eventid:", Intervenant);
          console.log("dans event ", info.event._def.extendedProps);
          if (input2.checked) {
            Informatisé = 0;
          } else {
            Informatisé = 1;
          }
          console.log("test", JSON.parse(sessionStorage["Mes_Crenaux"]));
          JSON.parse(sessionStorage["Mes_Crenaux"]).forEach((element) => {
            var x;
            var y;

            /*  x.push(element.extendedProps.CodeUE);
            x.push(element.extendedProps.Type);
            x.push(element.extendedProps.Groupe);
            x.push(element.start);
            x.push(element.end);
            y.push(info.event._def.extendedProps.CodeUE);
            y.push(info.event._def.extendedProps.Type);
            y.push(info.event._def.extendedProps.Groupe);
            y.push(info.event._instance.range.start);
            y.push(info.event._instance.range.end);*/
            y = info.event._def.extendedProps.Intervenant;
            x = element.extendedProps.Intervenant;
            var w = new Date(element.start).toISOString();
            var z = new Date(info.event._instance.range.start);
            z.setHours(z.getHours() - 2);
            var a = element.extendedProps.Groupe;
            var b = element.extendedProps.Groupe;
            console.log(x == y, x, y, w == z.toISOString(), w, z.toISOString());
          });
          info.event.setExtendedProp("Informatise", Informatisé);
          info.event.setExtendedProp("Intervenant", Intervenant);

          //info.event._def.extendedProps.Intervenant = Inter;
          console.log("here", info);
          console.log(
            "Intervenant : ",
            info.event._def.extendedProps.Intervenant
          );
          console.log("Infor : ", info.event._def.extendedProps.Informatise);
          /*  document.getElementById("ID_input1").style.display = "none";
        document.getElementById("ID_bouton1").style.display = "none";
        document.getElementsByName("ID_input2")[0].style.display = "none";
        document.getElementsByName("ID_input2")[1].style.display = "none";*/
          document.getElementById("popup-Form").style.display = "none";
          info.el.style.borderColor = border;
          i++;
        }
      }

      document
        .querySelector('[name="myForm"]')
        .addEventListener("submit", function (event) {
          event.preventDefault();
          //console.log(event);
        });

      //console.log(info.event);
    },
    headerToolbar: false,
    dayHeaderFormat: {
      weekday: "long",
    },
    initialView: "timeGridWeek",
    views: {
      timeGridWeek: {
        eventMinHeight: 15,
        allDaySlot: false,
        slotMinTime: "08:00:00",
        slotMaxTime: "20:00:00",
        slotDuration: "00:30:00",
        slotEventOverlap: false,
        expandRows: true,
      },
    },
    weekends: false,
    eventDurationEditable: false,
    initialDate: "2021-09-01",
    validRange: {
      start: toY_M_D(Mon_Week()),
      end: toY_M_D(Sat_Week()),
    },
    locale: "fr",
    editable: true,
    droppable: true,
    weekNumbers: true,
    eventOrder: "Type, Groupe",

    eventOverlap: function (stillEvent, movingEvent) {
      console.log("Event immobile :", stillEvent);
      console.log("Event mobile :", movingEvent);
      var sameInterv =
        stillEvent._def.extendedProps.Intervenant !=
        movingEvent._def.extendedProps.Intervenant;
      if (
        stillEvent._def.extendedProps.Intervenant == "" &&
        movingEvent._def.extendedProps.Intervenant == ""
      ) {
        sameInterv = 1;
      }
      var sameGroupe =
        stillEvent._def.extendedProps.Groupe !=
        movingEvent._def.extendedProps.Groupe;
      var CodeStill = stillEvent._def.extendedProps.CodeUE;
      var CodeMobile = movingEvent._def.extendedProps.CodeUE;
      console.log(CodeStill.slice(0, 3), CodeStill.slice(4, 7));
      var boolAnglais1 = 1;
      var boolAnglais2 = 1;
      if (CodeStill.slice(0, 3) == "HAL" && CodeStill.slice(4, 7) == "01L") {
        boolAnglais1 = 0;
      }
      if (CodeMobile.slice(0, 3) == "HAL" && CodeMobile.slice(4, 7) == "01L") {
        boolAnglais2 = 0;
      }
      var boolAnglais = boolAnglais1 == boolAnglais2;
      console.log("drop?", sameInterv, sameGroupe);
      return sameInterv && sameGroupe && boolAnglais;
    },

    drop: function (info) {
      uecheck = LiUE[info.draggedEl.id];
      wichCourse = course(info.draggedEl.childNodes[0].childNodes[0].innerText);
      switch (wichCourse) {
        case "CM":
          uecheck.ajoutCM();
          reloadEvents(liCodeUe, LiUE);
          break;

        case "TD":
          var grp = info.draggedEl.childNodes[0].childNodes[0].innerText
            .substr(3)
            .split(" ")[0];
          console.log("grp current ? ", grp);
          uecheck.ajoutTDGroupe(grp);
          reloadEvents(liCodeUe, LiUE);
          break;

        case "TP":
          var grp = info.draggedEl.childNodes[0].childNodes[0].innerText
            .substr(3)
            .split(" ")[0];
          uecheck.ajoutTPGroupe(grp);
          reloadEvents(liCodeUe, LiUE);
          break;

        default:
          alert("Erreur!");
          break;
      }
    },
    eventContent: (arg) => {
      if (!ferie(arg.event)) {
        ////console.log(arg.event.extendedProps.CodeUE);
        // console.log("from affichage : ", arg.event);
        return (
          arg.event.title.split(" ")[0] +
          " " +
          getcodeUE(arg.event) +
          " " +
          getduree(arg.event) +
          " " +
          arg.event._def.extendedProps.Intervenant +
          " " +
          (arg.event._def.extendedProps.Informatise ? "| Info" : "")
        );
      }
    },
  };
}
